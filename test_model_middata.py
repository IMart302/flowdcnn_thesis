# coding=utf-8

import serial_flownet as snet
import handy_flow_routines as futils
import flow_models_base as fmods
import os
import numpy as np
import averageepe as epe
import dataset_utils as dsutils
import time

cwd = os.getcwd()

#Dimetrodon 584x388
#Grove2 640x480
#Grove3 640x480
#Hydrangea 584x388
#RubberWhale 584x388
#Urban2 640x480
#Urban3 640x480
#Venus 420x380


##### 584x388
names_folders_ex = ["Dimetrodon",
                    "Hydrangea",
                    "RubberWhale"]
width=584
height=388
cost_flag=1

"""
#### 640x480 
names_folders_ex = ["Grove2",
                    "Grove3",
                    "Urban2",
                    "Urban3"]
width=640
height=480
cost_flag=2

#### 420x380
names_folders_ex = ["Venus"]
width=420
height=380
cost_flag=3
"""

data_path = "other-data"
gt_data = "other-gt-flow"

name = "SNET_DEBUG_L1LOSS_1000EX_DUNET_LR_00001"  #here!
models_ckpt_paths = ["N1"]
mflownet = snet.FlowNetStackManager(name)
modlist = [fmods.FlowNetBrickModel2()]       #here!
mflownet.model_build(height, width, modlist, False)
mflownet.init_session()
mflownet.model_session_chkpt_restore(models_ckpt_paths)

output_data_dir = cwd +"/" +name + "_data_out_othermbury"
if(not os.path.exists(output_data_dir)):
    os.mkdir(output_data_dir)

cost_log = open(output_data_dir+ "/" +"cost_log_"+str(cost_flag)+".txt", "w")

print("####################################### \n")
print(name)
print("\n####################################### \n")

for ex in names_folders_ex:
    f1 = cwd+"/"+data_path+"/"+ex+"/frame10.png"
    f2 = cwd+"/"+data_path+"/"+ex+"/frame11.png"
    gt = cwd+"/"+gt_data+"/"+ex+"/flow10.flo"
    flow = futils.readFlow(gt)
    img1 = futils.readImage(f1)
    img2 = futils.readImage(f2)
    flowes = np.array(mflownet.inference(img1, img2))
    flowes = np.reshape(flowes, [flowes.shape[2], flowes.shape[3], flowes.shape[4]])
    futils.writeFlow(output_data_dir + "/" + ex + ".flo", flowes)
    e = epe.np_average_endpoint_error(flowes, flow)
    cost_log.write(ex +","+ str(e)+"\n")
mflownet.close_session()

