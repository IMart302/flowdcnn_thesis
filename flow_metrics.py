import tensorflow as tf

#calcula el AEE que se usa como Loss
#los tensores deben ser en formato [batch, height, width, channel]
def average_endpoint_error(prediction, label):
    with tf.name_scope(None, "average_endpoint_error", [prediction, label]):

        #pred_shape = prediction.get_shape()

        #(x_pred - x_lab)^2 -> C1
        #(y_pred - y_lab)^2 -> C2
        temp = tf.square(tf.subtract(prediction, label))
        
        #suma los canales, es decir en dimension 3
        #el tensor ahora es [batch, row, col]
        sums = tf.reduce_sum(temp, axis=3)
        cost = tf.sqrt(sums)
        cost = tf.reduce_mean(cost, axis=[1,2]) 
        
        cost = tf.reduce_sum(cost)
        
        #cost = tf.losses.mean_squared_error(label, prediction)
        #cost = tf.losses.absolute_difference(label, prediction)
        tf.losses.add_loss(cost)
        return cost


def l1_error(prediction, label):
    #cost = tf.losses.mean_squared_error(label, prediction)
    cost = tf.losses.absolute_difference(label, prediction)
    return cost

def l2_error(prediction, label):
    #cost = tf.losses.mean_squared_error(label, prediction)
    cost = tf.losses.mean_squared_error(label, prediction)
    return cost