# coding=utf-8

import serial_flownet as snet
import handy_flow_routines as futils
import flow_models_base as fmods
import os
import numpy as np
import averageepe as epe
import dataset_utils as dsutils
import time

#windows
#dataset_path = "C:\LinuxNexum\Documents\FlyingChairs_release\data"
#linux
dataset_path = "/home/ivan/Datasets/FlyingChairs_release/data"
#turriza
#dataset_path = ""

cwd = os.getcwd()

name = "SNET_Adam_L1LOSS_5000EX_B20_DILATFLOWNET_LR_0001"  #here!
models_ckpt_paths = ["N1", "N2"]
mflownet = snet.FlowNetStackManager(name)
modlist = [fmods.FlowNetBrickModel5(), fmods.FlowNetBrickModel5()]       #here!
train_list = [False, False]
mflownet.model_build(384, 512, modlist, last_trainable=False, trainables_list=train_list)
mflownet.init_session()
mflownet.model_session_chkpt_restore(models_ckpt_paths)

#indexes1 = [[1122, 1122], [1127, 1127], [1129, 1129], [1139, 1139], [1143, 1143], [1151, 1151], [1181, 1181], [1196, 1196], [1214, 1214]]
indexes2 = [[321, 400]]

indexes = indexes2


indxlist = []
for ran in indexes :
    indxlist = indxlist + list(range(ran[0], ran[1]+1))

output_data_dir = cwd +"/" +name + "_data_outp2"
if(not os.path.exists(output_data_dir)):
    os.mkdir(output_data_dir)

cost_log = open(output_data_dir+ "/" +"cost_log.txt", "w")

print("####################################### \n")
print(name)
print("\n####################################### \n")

for indx in indxlist:
    img1, img2, flow = dsutils.ChairsDatasetManager.loadSampleAt(dataset_path, indx)
    flowes = np.array(mflownet.inference(img1, img2))
    flowes = np.reshape(flowes, [flowes.shape[2], flowes.shape[3], flowes.shape[4]])
    futils.writeFlow(output_data_dir + "/" + str(indx) + ".flo", flowes)
    e = epe.np_average_endpoint_error(flowes, flow)
    cost_log.write(str(indx) +","+ str(e)+"\n")

mflownet.close_session()

