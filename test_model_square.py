# coding=utf-8

import serial_flownet as snet
import handy_flow_routines as futils
import flow_models_base as fmods
import os
import numpy as np
import averageepe as epe
import dataset_utils as dsutils
import time

cwd = os.getcwd()
dataset_path=os.getcwd()+"/"+"simple_square_flow_dateset_12" #HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

train = [[1, 2000]]               
#test = [[2001, 2600]]                      #HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
test = [[3001, 3600]]

name = "SQFNET_DS12_Adam_L1LOSS_2000EX_B20_DUNET_LR_00001"  #HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
models_ckpt_paths = ["N1"]
mflownet = snet.FlowNetStackManager(name)
modlist = [fmods.FlowNetBrickModel2()]                            #HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
mflownet.model_build(150, 150, modlist, last_trainable=False)
mflownet.init_session()
mflownet.model_session_chkpt_restore(models_ckpt_paths)

indexes = test


indxlist = []
for ran in indexes :
    indxlist = indxlist + list(range(ran[0], ran[1]+1))

output_data_dir = cwd +"/" +name + "test_out"
if(not os.path.exists(output_data_dir)):
    os.mkdir(output_data_dir)

cost_log = open(output_data_dir+ "/" +"cost_log.txt", "w")

print("####################################### \n")
print(name)
print("\n####################################### \n")

for indx in indxlist:
    img1, img2, flow = dsutils.SquareColorDatasetManager.loadSampleAt(dataset_path, indx, subfixes=["_img1.png", "_img2.png", "_flow.flo"])
    img1 = futils.normalize_image(img1)
    img2 = futils.normalize_image(img2)
    flowes = np.array(mflownet.inference(img1, img2))
    flowes = np.reshape(flowes, [flowes.shape[2], flowes.shape[3], flowes.shape[4]])
    futils.writeFlow(output_data_dir + "/" + str(indx) + ".flo", flowes)
    e = epe.np_average_endpoint_error(flowes, flow)
    cost_log.write(str(indx) +","+ str(e)+"\n")

mflownet.close_session()