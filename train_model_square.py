# coding=utf-8

import serial_flownet as snet
import flow_models_base as fmods 
import dataset_utils as fds
import os
import time

optimizer="ADAM"
learning_rate=0.00001
dataset_path=os.getcwd()+"/"+"simple_square_flow_dateset_12"

train = [[1, 3000]]
test = [[3001, 3600]]
batchsize = 20

"""
name_model = "SQUAREFNET_Adam_L1LOSS_1000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SQFNET_DS2_Adam_L1LOSS_1000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""
"""
name_model = "SQFNET_DS2_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""
"""
name_model = "SQFNET_DS2_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SQFNET_DS3_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""

"""
name_model = "SQFNET_DS3_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SQFNET_2_DS4_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""

"""
name_model = "SQFNET_DS4_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""
"""
name_model = "SQFNET_DS5_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SQFNET_DS5_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""
"""
name_model = "SQFNET_D1_1_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SQFNET_D1_1_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""

"""
name_model = "SQFNET_DS7_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SQFNET_DS7_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""
"""
name_model = "SQFNET_DS8_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SQFNET_DS8_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""
"""
name_model = "SQFNET_DS9_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SQFNET_DS9_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""
"""
name_model = "SQFNET_DS10_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SQFNET_DS10_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""
"""
name_model = "SQFNET_DS11_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SQFNET_DS11_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""
"""
name_model = "SQFNET_DS12_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""

name_model = "SQFNET_DS12_Adam_L1LOSS_2000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":optimizer, "learning_rate": learning_rate, "cost_log_write_batch": 20}
save_schedule = {"save_epoch":1, "save_batch_step":50}
ds = fds.SquareColorDatasetManager(dataset_path, train, test, batchsize, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]


shape = ds.getFinalShape() #height, width
mflownet.model_build(shape[0], shape[1], mods, last_trainable=True)
mflownet.init_session()
mflownet.model_session_chkpt_restore(models_ckpt_paths)
#mflownet.train(chairs_ds, train_schedule, save_schedule)
mflownet.train2(ds, train_schedule, save_schedule) #fix gpu lack of memory
mflownet.close_session()