import numpy as np 

#numpy arrays shape [rows, cols, channels]
def np_average_endpoint_error(prediction, target):

    norms = np.sqrt(np.sum(np.square(np.subtract(prediction, target)), axis=2))
    avepe = np.mean(norms)
    return avepe