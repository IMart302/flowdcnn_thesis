# coding=utf-8

import stack_flownet as snet
import handy_flow_routines as futils
import flow_models_base as fmods
import os
import numpy as np

#dataset_path = "C:\LinuxNexum\Documents\FlyingChairs_release\data"

imgs1 = ["0000000-img0.ppm", "0000001-img0.ppm", "0000002-img0.ppm", "0000003-img0.ppm", "0000004-img0.ppm", "0000005-img0.ppm", "0000006-img0.ppm"]
imgs2 = ["0000000-img1.ppm", "0000001-img1.ppm", "0000002-img1.ppm", "0000003-img1.ppm", "0000004-img1.ppm", "0000005-img1.ppm", "0000006-img1.ppm"]

dataset_path = "/home/ivan/Datasets/FlyingChairs_release/data"
#frame1_path = "/home/ivan/Datasets/FlyingChairs_examples/0000000-img0.ppm"
#frame2_path = "/home/ivan/Datasets/FlyingChairs_examples/0000000-img1.ppm"
#frame1_path = "/home/ivan/Datasets/FlyingChairs_release/data/00004_img1.ppm"
#frame2_path = "/home/ivan/Datasets/FlyingChairs_release/data/00004_img2.ppm"
frame_path = "/home/ivan/Datasets/FlyingChairs_examples/"

models_ckpt_paths = ["N1"]

mflownet = snet.FlowNetStackManager("STM5")

modlist = [fmods.NaiveFlowNetModel2()]
frame1 = futils.read(frame_path+imgs1[0])
mflownet.model_build(frame1.shape[0], frame1.shape[1], modlist, False)

mflownet.init_session()
mflownet.model_session_chkpt_restore(models_ckpt_paths)


for indx in range(0, len(imgs1)):
    frame1 = futils.read(frame_path+imgs1[indx])
    frame2 = futils.read(frame_path+imgs2[indx])
    flowes = np.array(mflownet.inference(frame1, frame2))
    print(flowes.shape)
    flowes = np.reshape(flowes, [flowes.shape[2], flowes.shape[3], flowes.shape[4]])
    futils.writeFlow("test_flow_examples" + str(indx) + ".flo", flowes)

mflownet.close_session()

