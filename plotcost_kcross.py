import numpy as np 
import matplotlib.pyplot as plt
import csv
import os 

"""
file1 = "STM5_debugK1_cost_log_2019-02-12-21-34-22.txt"
file2 = "STM5_debugK2_cost_log_2019-02-13-00-46-34.txt"
file3 = "STM5_debugK3_cost_log_2019-02-13-04-55-49.txt"
file4 = "STM5_debugK4_cost_log_2019-02-13-08-13-21.txt"
"""

#file1 = "STM5_debugK1_200_cost_log_2019-02-18-05-45-11.txt"
file1 = "STM5_debugK2_200_cost_log_2019-02-18-22-24-06.txt"
#file1 = "STM5_debugK3_200_cost_log_2019-02-19-20-03-59.txt"

cwd = os.getcwd()
srows = 6

cost1 = []
test1 = []

cost2 = []
test2 = []

cost3 = []
test3 = []

cost4 = []
test4 = []

filename1=cwd+"/"+file1
#filename2=cwd+"/"+file2
#filename3=cwd+"/"+file3
#filename4=cwd+"/"+file4

xi = 0
rcount = 0

"""
filenames = [filename1, filename2, filename3, filename4]
costs = [cost1, cost2, cost3, cost4]
tests = [test1, test2, test3, test4]
"""
filenames = [filename1]
costs = [cost1]
tests = [test1]

k = 0
for filename in filenames:
    with open(filename,'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        for row in plots:
            if(rcount < srows):
                rcount = rcount + 1
            else:
                costs[k].append(float(row[0]))
                tests[k].append(float(row[1]))
    
    rcount=0
    k = k + 1

x = list(range(0, len(costs[0])))

for i in range(0, len(costs)):
    plt.plot(x, costs[i], label="entrenamiento")
    plt.plot(x, tests[i], label="prueba")
    plt.xlabel('Epoca')
    plt.title(file1)
    plt.legend()
    plt.show()


costav = []
testav = []

"""
for i in range(0, len(cost1)):
    costav.append((cost1[i] + cost2[i] + cost3[i] + cost4[i])/4.0)
    testav.append((test1[i] + test2[i] + test3[i] + test4[i])/4.0)


plt.plot(x, costav, label="entrenamiento")
plt.plot(x, testav, label="prueba")
plt.xlabel('Epoca')
plt.title("Promedio K Folds")
plt.legend()
plt.show()

"""