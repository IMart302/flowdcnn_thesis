# coding=utf-8

import tensorflow as tf
import numpy as np 
import os
import flow_models_base as fmod
import  flow_metrics as fmet
import time
import sys

#define el modelo y métodos para utlizarlo y manejo de sesiones
#y modelos en stack
class FlowNetStackManager(object):
    def __init__(self, name):
        self.stack = -1
        self.name = name
        self.nets = []
        self.ph_X = None
        self.ph_Y = None
        self.predic = None
        self.session = None
        self.models_loaded = False
        self.vars_to_init = False
        self.savers = None
        self.init_last = True
        self.cost = None
        self.dropout_act = False
        self.dropout = 1.0
        self.keep_prob = None
        self.trainable_list = None
        cwdir = os.getcwd()
        self.amod = cwdir+"/"+self.name
    
    #trinable list debe ser una lista de bools
    def model_build(self, H_shape, W_shape, models_base_list, last_trainable=None, make_cost = False, dropout = None, trainables_list = None):
        if(len(models_base_list) == 0):
            print("La lista de modelos esta vacia")
            return

        self.stack = len(models_base_list)

        #para seleccionar que modelos se entrenan simultaneamente
        
        train_previous = False
        if(trainables_list != None):
            if(len(trainables_list) == len(models_base_list)):
                train_previous = True
                if(trainables_list[-1] != last_trainable):
                    print("El ultimo elemento de la lista de last_trainables debe coincidir con last trainable")
                    sys.exit()

                self.trainable_list = trainables_list

        #define los placeholder para alimentar el modelo en stack
        self.ph_X = tf.placeholder(tf.float32, name="flowfeed", shape=[None, H_shape, W_shape, 6])
        self.ph_Y = tf.placeholder(tf.float32, name="flowlabel", shape=[None, H_shape, W_shape, 2])
        #for models that require dropout, even if doesnt use it
        self.keep_prob = tf.placeholder(tf.float32, name="keep_prob")
        self.dropout = dropout
    
        if(self.stack == 1):
            net = models_base_list[0]
            net.build_model(self.ph_X, 6, "N"+str(1), trainable=last_trainable, keep_prob=self.keep_prob)
            self.nets.append(net)
            self.predic = net.getOutputTensor()
        else:
            net = models_base_list[0]
            if(train_previous):
                net.build_model(self.ph_X, 6, "N"+str(1),  keep_prob=self.keep_prob, trainable=trainables_list[0])
            else:
                net.build_model(self.ph_X, 6, "N"+str(1),  keep_prob=self.keep_prob, trainable=False)
            
            self.nets.append(net)
            
            for i in range(0, self.stack - 2):
                net = models_base_list[i+1]
                #concatena el placeholder de entrada con la differencia de flujo predecido con el ground truth
                flow_diff = self.nets[-1].getOutputTensor()
                conc = tf.concat([self.ph_X, flow_diff], axis=3)
                #los siguientes modelos reciben 8 canales de entrada 3+3+2
                if(train_previous):
                    net.build_model(conc, 8, "N"+str(i+2), keep_prob=self.keep_prob, trainable=trainables_list[i+1])
                else:
                    net.build_model(conc, 8, "N"+str(i+2), keep_prob=self.keep_prob, trainable=False)
                self.nets.append(net)
            
            flow_diff = self.nets[-1].getOutputTensor()
            conc = tf.concat([self.ph_X, flow_diff], axis=3)
            net = models_base_list[-1]
            net.build_model(conc, 8, "N"+str(self.stack), trainable=last_trainable, keep_prob=self.keep_prob)
            self.nets.append(net)
            self.predic = net.getOutputTensor()
        
        if(make_cost == True):
            self.cost = self.nets[-1].makeLoss(self.ph_Y)


    def init_session(self):
        print("Iniciando sesion")
        self.session = tf.Session()

    def close_session(self):
        if(self.session != None):
            self.session.close()
            self.session = None

    def model_session_chkpt_restore(self, checkpoints_models_paths):
        if(self.session == None):
            print("No se ha iniciado sesion")
            return
        #Verifica primero que existan todos los archivos de
        #los modelos a restaurar y si el último
        #es primera vez que se entrena o no
        cwdir = os.getcwd()
        amod = cwdir+"/"+self.name
        if(not os.path.exists(amod)):
            if(self.stack == 1):
                #si es primera vez que sea crea el modelo de stack
                #y por tanto no hay ningun modelo
                os.mkdir(amod)
            else:
                print("No hay directorio en: "+self.name)
                return
        
        if(len(checkpoints_models_paths) < self.stack - 2):
            print("Se necesitan los checkpoints de los modelos anteriores")
            return
        
        #el modelo al final del stack ha sido creado por tanto existe el ckpt
        #y se carga, si no, el modelo es nuevo al final, y si existiese será borrado
        if(len(checkpoints_models_paths) == self.stack):
            p = amod+"/"+"N"+str(self.stack)
            if(not os.path.exists(p)):
                os.mkdir(p)
            
            print("INIT LAST FALSE")
            self.init_last = False 
        
        #verifica que los chkpoint existan
        for p in checkpoints_models_paths:
            if(not os.path.exists(amod+"/"+p)):
                print("El checkpoint en "+p+" no existe")
                return
    
        #savers para guardar o restaurar las variables de cada modulo en diferentes archivos
        self.savers = []
        for i in range(0, self.stack):    
            self.savers.append(tf.train.Saver(self.nets[i].getAllVars() + [self.nets[i].get_local_step()]))
            
        #Restaura los modulos de variables
        for i in range(0, len(checkpoints_models_paths)):
            #<global_path>/<Model name>/<Ni>/file.chkpt
            p1 = amod+"/"+checkpoints_models_paths[i]+"/naive_model.ckpt"
            self.savers[i].restore(self.session, p1)
            print("model: " + p1 + " restored")

        self.models_loaded = True


    def train(self, dataset_manager, train_schedule, save_schedule, custom_train = False):
        if(custom_train == True):
            self.train_custom(dataset_manager, train_schedule, save_schedule)
            return

        if(self.session == None):
            print("No se ha iniciado sesion")
            return

        cwdir = os.getcwd()
        amod = cwdir+"/"+self.name

        actual_step_counter = self.nets[-1].get_local_step()

        if(self.cost == None):
            self.cost = self.nets[-1].makeLoss(self.ph_Y)

        if "learning_rate" in train_schedule:
            learning_rate = train_schedule["learning_rate"]
        else:
            learning_rate = 0.001

        opt_select = "SGD"
        if "optimizer" in train_schedule:
            opt_select = train_schedule["optimizer"]
            if(opt_select != "SGD" and opt_select != "ADAM" and opt_select != "RMSProp" and opt_select != "Adadelta"):
                opt_select = "SGD"
        
        if(opt_select == "SGD"):
            optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
        elif(opt_select == "ADAM"):
            optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        elif(opt_select == "RMSProp"):
            optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate)
        elif(opt_select == "Adadelta"):
            optimizer = tf.train.AdadeltaOptimizer(learning_rate=learning_rate)
        
        vars_to_optimize = self.nets[-1].getAllVars()
        train_op = optimizer.minimize(self.cost, global_step=actual_step_counter, var_list=vars_to_optimize)
        
        #selecciona que variables se inicializan
        vars_to_init = []

        if(self.init_last == True):
            vars_to_init = vars_to_init + vars_to_optimize + [actual_step_counter]
        
        if(opt_select != "SGD"):
            #variables no inicializadas del optimizador
            opt_vars = [optimizer.get_slot(var, name)
                        for name in optimizer.get_slot_names()
                        for var in vars_to_optimize if var is not None]
            if(opt_select=="ADAM"):
                opt_vars.extend(list(optimizer._get_beta_accumulators()))
            
            vars_to_init = vars_to_init + opt_vars
        
        #vars_to_init = vars_to_init + opt_vars
        init_vars = tf.initialize_variables(vars_to_init)

        print("===VARIABLES TO INITIALIZE===")
        for var in vars_to_init:
            print(var.name)
        print("=============================")
        print(opt_select)

        epochs = train_schedule["epochs"]
        #save_epoch = save_schedule["save_epoch"]
        save_batch_step = save_schedule["save_batch_step"]
              
        if("batch_display" in train_schedule):
            batch_display = train_schedule["batch_display"]
        else:
            batch_display = 5

        if("cost_log_write_batch" in train_schedule):
            write_to_log_batch = train_schedule["cost_log_write_batch"]
        else:
            write_to_log_batch = 10
        
        if("cost_limit_examples" in train_schedule):
            cost_limit_examples = train_schedule["cost_limit_examples"]
        else:
            cost_limit_examples = None
        
        max_batches = dataset_manager.getAvailableTrainBatches()
        if("batches_lim" in train_schedule):
            batch_lim = train_schedule["batches_lim"]
            if(batch_lim > max_batches):
                batches = max_batches
            else:
                batches = batch_lim
        else:
            batches = max_batches

        
        #inicializacion de varaibles requeridas
        print("==========Inicializando variables==================")
        self.session.run(init_vars)
        cost_log = open(self.name+"_cost_log_"+str(time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime()))+".txt", "w")
        cost_log.write("Name: "+self.name+"\n")
        cost_log.write("Epocs: "+str(epochs)+"\n")
        cost_log.write("Batches: "+str(batches)+"\n")
        cost_log.write("Learning Rate: "+str(learning_rate)+"\n")
        cost_log.write("Date: "+str(time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime()))+"\n")
        cost_log.write("train test \n")
        #begin training
        
        print("epocas: "+str(epochs))
        print("batches: "+str(batches))
        print("cost limit of examples: " +str(cost_limit_examples))
        print("name: " + self.name)

        #calculate train/test cost
        costt, costv = self.calculate_train_test_cost(dataset_manager, cost_limit_examples=cost_limit_examples)
        cost_log.write(str(costt)+","+str(costv)+"\n")
        
        dataset_manager.reset_indx()
        dataset_manager.shuffleTrain()
        
        for e in range(0, epochs):
            print("Epoca: " +str(e))
            for b in range(0, batches):
                
                #obten el sieguiente ejemplo de entrenamiento
                imgs_feed, flow_labels = dataset_manager.nextFlowBatch()
                #realiza paso de optimizacion

                self.session.run(train_op, feed_dict={self.ph_X: imgs_feed, self.ph_Y: flow_labels, self.keep_prob:self.dropout})
                
                #muestra en consola el costo en cierto paso de batch
                if(b % batch_display == 0):
                    #obten el costo actualizado con los pesos
                    c = self.session.run(self.cost, feed_dict={self.ph_X: imgs_feed, self.ph_Y: flow_labels, self.keep_prob: 1.0})    
                    print("Epoca: "+str(e)+" | Paso de batch: "+ str(b) + " | Costo: " + str(c))
                    #cost_log.write(str(c)+"\n")
                
                #guarda el modelo cada cierto paso de entrenamiento
                if(b % save_batch_step == 0):
                    p = amod+"/"+"N"+str(self.stack)+"/naive_model.ckpt"
                    self.savers[-1].save(self.session, p)
                    print("Modelo guardado")
                
                #guarda en el archivo log el costo actual cada cierto paso de batch
                if(write_to_log_batch != None):
                    if(b % write_to_log_batch == 0):
                        costt, costv = self.calculate_train_test_cost(dataset_manager, cost_limit_examples=cost_limit_examples)
                        cost_log.write(str(costt)+","+str(costv)+"\n")
            

            dataset_manager.shuffleTrain()
            dataset_manager.reset_indx()
                
        p = amod+"/"+"N"+str(self.stack)+"/naive_model.ckpt"
        self.savers[-1].save(self.session, p)
        print("Modelo guardado")
        cost_log.close()


    #fix memory oom on gpu for larger batch size
    def train2(self, dataset_manager, train_schedule, save_schedule, custom_train = False):
        if(custom_train == True):
            self.train_custom(dataset_manager, train_schedule, save_schedule)
            return

        if(self.session == None):
            print("No se ha iniciado sesion")
            return

        cwdir = os.getcwd()
        amod = cwdir+"/"+self.name

        actual_step_counter = self.nets[-1].get_local_step()

        if(self.cost == None):
            self.cost = self.nets[-1].makeLoss(self.ph_Y)

        if "learning_rate" in train_schedule:
            learning_rate = train_schedule["learning_rate"]
        else:
            learning_rate = 0.0001

        opt_select = "SGD"
        if "optimizer" in train_schedule:
            opt_select = train_schedule["optimizer"]
            if(opt_select != "SGD" and opt_select != "ADAM" and opt_select != "RMSProp" and opt_select != "Adadelta"):
                opt_select = "SGD"
        
        
        batchsize=dataset_manager.getBatchSize()

        #vars for accumulate gradients
        #vars_to_optimize = self.nets[-1].getAllVars()
        vars_to_optimize = tf.trainable_variables()
        print(vars_to_optimize)
        accum_vars = [tf.Variable(tf.zeros_like(tv.initialized_value()), trainable=False) for tv in vars_to_optimize]
        #set to zero te acummulators operations
        zero_ops = [tv.assign(tf.zeros_like(tv)) for tv in accum_vars]

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            if(opt_select == "SGD"):
                optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
            elif(opt_select == "ADAM"):
                optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
            elif(opt_select== "RMSProp"):
                optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate)
            elif(opt_select=="Adadelta"):
                optimizer = tf.train.AdadeltaOptimizer(learning_rate=learning_rate)

            #compute gradients operation in graph
            gvs = optimizer.compute_gradients(self.cost, vars_to_optimize)
            #accummulate gradient operations
            accum_ops = [accum_vars[i].assign_add(gv[0]) for i, gv in enumerate(gvs)]
            #apply gradient operation
            train_op = optimizer.apply_gradients([(accum_vars[i]/float(batchsize), gv[1]) for i, gv in enumerate(gvs)])
        
        #selecciona que variables se inicializan
         
        vars_to_init = accum_vars

        if(self.init_last == True):
            vars_to_init = vars_to_init + vars_to_optimize + [actual_step_counter]
        
        if(opt_select != "SGD"):
            #variables no inicializadas del optimizador
            opt_vars = [optimizer.get_slot(var, name)
                        for name in optimizer.get_slot_names()
                        for var in vars_to_optimize if var is not None]
            if(opt_select=="ADAM"):
                opt_vars.extend(list(optimizer._get_beta_accumulators()))
            
            vars_to_init = vars_to_init + opt_vars

        #vars_to_init = vars_to_init + opt_vars
        init_vars = tf.initialize_variables(vars_to_init)

        print("===VARIABLES TO INITIALIZE===")
        for var in vars_to_init:
            print(var.name)
        print("=============================")
        print(opt_select)

        epochs = train_schedule["epochs"]
        #save_epoch = save_schedule["save_epoch"]
        save_batch_step = save_schedule["save_batch_step"]
              
        if("batch_display" in train_schedule):
            batch_display = train_schedule["batch_display"]
        else:
            batch_display = 5

        if("cost_log_write_batch" in train_schedule):
            write_to_log_batch = train_schedule["cost_log_write_batch"]
        else:
            write_to_log_batch = 10
        
        if("cost_limit_examples" in train_schedule):
            cost_limit_examples = train_schedule["cost_limit_examples"]
        else:
            cost_limit_examples = None
        
        max_batches = dataset_manager.getAvailableTrainBatches()
        if("batches_lim" in train_schedule):
            batch_lim = train_schedule["batches_lim"]
            if(batch_lim > max_batches):
                batches = max_batches
            else:
                batches = batch_lim
        else:
            batches = max_batches

        
        #inicializacion de varaibles requeridas
        print("==========Inicializando variables==================")
        self.session.run(init_vars)
        cost_log = open(self.name+"_cost_log_"+str(time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime()))+".txt", "w")
        cost_log.write("Name: "+self.name+"\n")
        cost_log.write("Epocs: "+str(epochs)+"\n")
        cost_log.write("Batches: "+str(batches)+"\n")
        cost_log.write("Learning Rate: "+str(learning_rate)+"\n")
        cost_log.write("Date: "+str(time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime()))+"\n")
        cost_log.write("train test \n")
        #begin training
        
        print("epocas: "+str(epochs))
        print("batches: "+str(batches))
        print("cost limit of examples: " +str(cost_limit_examples))
        print("name: " + self.name)

        #calculate train/test cost
        costt, costv = self.calculate_train_test_cost2(dataset_manager, cost_limit_examples=cost_limit_examples)
        #costt, costv = 0, 0
        print("train cost: "+str(costt)+" test cost: "+str(costv))
        cost_log.write(str(costt)+","+str(costv)+"\n")
        
        dataset_manager.reset_indx()
        dataset_manager.shuffleTrain()
        
        mini_batch = dataset_manager.getBatchSize()
        for e in range(0, epochs):
            print("Epoca: " +str(e))
            for b in range(0, batches):
                
                #obten el sieguiente ejemplo de entrenamiento en una lista
                imgs_feed, flow_labels = dataset_manager.nextFlowBatch(in_list=True)
                
                #paso de entrenamiento empieza
                #poner a cero los acumuladores
                print("CALCULANDO GRADIENTES")
                self.session.run(zero_ops)
                for i in range(0, mini_batch):
                    #acumular el gradiente
                    self.session.run(accum_ops, feed_dict={self.ph_X: imgs_feed[i:(i+1), :, :, :], self.ph_Y: flow_labels[i:(i+1), :, :, :], self.keep_prob: self.dropout})
                #el ultimo paso, acumula, normaliza y aplica los gradientes,porque de nuevo ejecuta todo el grafo para ese ejemplo
                #self.session.run(train_op, feed_dict={self.ph_X: imgs_feed[np.newaxis, i, :, :, :], self.ph_Y: flow_labels[np.newaxis, i, :, :, :], self.keep_prob: self.dropout})
                self.session.run(train_op)
                print("APPLIED GRADS")
                #finaliza paso de entrenamiento
                
                #guarda el modelo cada cierto paso de entrenamiento
                if(b % save_batch_step == 0):
                    self.save_models_now()
                
                #guarda en el archivo log el costo actual cada cierto paso de batch
                if(write_to_log_batch != None):
                    if(b % write_to_log_batch == 0):
                        costt, costv = self.calculate_train_test_cost2(dataset_manager, cost_limit_examples=cost_limit_examples)
                        print("Epoca: " + str(e) + " batch step: "+str(b)+" train cost: "+str(costt)+" test cost: "+str(costv))
                        cost_log.write(str(costt)+","+str(costv)+"\n")
            

            dataset_manager.shuffleTrain()
            dataset_manager.reset_indx()
                
        self.save_models_now()

        cost_log.close()


    #no usar externamente, debe ser private
    def calculate_train_test_cost(self, dataset_manager, cost_limit_examples = None):
        
        #guarda y ordena los indices
        if(cost_limit_examples == None):
            dataset_manager.saveActualIndexes(sort_change=False)
        else:
            dataset_manager.saveActualIndexes()
        
        dataset_manager.reset_indx()
        
        cost_train = 0
        batches_train = dataset_manager.getAvailableTrainBatches()
        batches_train_f = batches_train

        if(cost_limit_examples != None):
            if(batches_train >= cost_limit_examples):
                batches_train_f = cost_limit_examples

        for b in range(0, batches_train_f):
            imgs_feed, flow_labels = dataset_manager.nextFlowBatch()
            c = self.session.run(self.cost, feed_dict={self.ph_X: imgs_feed, self.ph_Y: flow_labels, self.keep_prob: 1.0})
            cost_train = cost_train + c

        cost_train = cost_train/float(batches_train_f)
        

        #calculate test cost
        cost_test = 0
        batches_test = dataset_manager.getAvailableValBatches()
        for b in range(0, batches_test):
            imgs_feed, flow_labels = dataset_manager.nextValBatch()
            c = self.session.run(self.cost, feed_dict={self.ph_X: imgs_feed, self.ph_Y: flow_labels, self.keep_prob: 1.0})
            cost_test = cost_test + c
        
        cost_test = cost_test/float(batches_test)

        if(cost_limit_examples == None):
            dataset_manager.restorePreviousIndexes(sort_change=False)
        else:
            dataset_manager.restorePreviousIndexes()

        return cost_train, cost_test

    #fix oom gpu, for large batch
    def calculate_train_test_cost2(self, dataset_manager, cost_limit_examples = None):
        
        #guarda y ordena los indices
        if(cost_limit_examples == None):
            dataset_manager.saveActualIndexes(sort_change=False)
        else:
            dataset_manager.saveActualIndexes()
        
        dataset_manager.reset_indx()
        
        cost_train = 0
        batches_train = dataset_manager.getAvailableTrainBatches()
        batches_train_f = batches_train

        if(cost_limit_examples != None):
            if(batches_train >= cost_limit_examples):
                batches_train_f = cost_limit_examples

        print("CALULANDO TRAIN")
        mini_batch = dataset_manager.getBatchSize()
        for b in range(0, batches_train_f):
            imgs_feed, flow_labels = dataset_manager.nextFlowBatch()
            for i in range(0, mini_batch):
                c = self.session.run(self.cost, feed_dict={self.ph_X: imgs_feed[i:(i+1), :, :, :], self.ph_Y: flow_labels[i:(i+1), :, :, :], self.keep_prob: 1.0})
                cost_train = cost_train + c
            
        cost_train = cost_train/float(batches_train_f*mini_batch)
        #calculate test cost
        cost_test = 0
        print("CALULANDO TEST")
        batches_test = dataset_manager.getAvailableValBatches()
        for b in range(0, batches_test):
            imgs_feed, flow_labels = dataset_manager.nextValBatch()
            c = self.session.run(self.cost, feed_dict={self.ph_X: imgs_feed, self.ph_Y: flow_labels, self.keep_prob: 1.0})
            cost_test = cost_test + c
        
        cost_test = cost_test/float(batches_test)

        if(cost_limit_examples == None):
            dataset_manager.restorePreviousIndexes(sort_change=False)
        else:
            dataset_manager.restorePreviousIndexes()

        return cost_train, cost_test



    def train_custom(self, dataset_manager, train_schedule, save_schedule):
        if(self.session == None):
            print("No se ha iniciado sesion")
            return

        cwdir = os.getcwd()
        amod = cwdir+"/"+self.name

        actual_step_counter = self.nets[-1].get_local_step()

        if(self.cost == None):
            self.cost = self.nets[-1].makeLoss(self.ph_Y)

        if "learning_rate" in train_schedule:
            learning_rate = train_schedule["learning_rate"]
        else:
            learning_rate = 0.001

        opt_select = "SGD"
        if "optimizer" in train_schedule:
            opt_select = train_schedule["optimizer"]
            if(opt_select != "SGD" and opt_select != "ADAM"):
                opt_select = "SGD"
        
        if(opt_select == "SGD"):
            optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
        elif(opt_select == "ADAM"):
            optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        
        vars_to_optimize = self.nets[-1].getAllVars()
        train_op = optimizer.minimize(self.cost, global_step=actual_step_counter, var_list=vars_to_optimize)
        
        #selecciona que variables se inicializan
        vars_to_init = []

        if(self.init_last == True):
            vars_to_init = vars_to_init + vars_to_optimize + [actual_step_counter]
        
        if(opt_select == "ADAM"):
            #variables no inicializadas del optimizador
            opt_vars = [optimizer.get_slot(var, name)
                        for name in optimizer.get_slot_names()
                        for var in vars_to_optimize if var is not None]
            opt_vars.extend(list(optimizer._get_beta_accumulators()))
            #opt_vars = optimizer.variables() no esta en tensorflow 1.13
            vars_to_init = vars_to_init + opt_vars

        
        #vars_to_init = vars_to_init + opt_vars
        init_vars = tf.initialize_variables(vars_to_init)

        print("===VARIABLES TO INITIALIZE===")
        for var in vars_to_init:
            print(var.name)
        print("=============================")
        print(opt_select)

        epochs = train_schedule["epochs"]
        #save_epoch = save_schedule["save_epoch"]
        save_batch_step = save_schedule["save_batch_step"]
              
        if("batch_display" in train_schedule):
            batch_display = train_schedule["batch_display"]
        else:
            batch_display = 5

        if("cost_log_write_batch" in train_schedule):
            write_to_log_batch = train_schedule["cost_log_write_batch"]
        else:
            write_to_log_batch = 10
        
        if("cost_limit_examples" in train_schedule):
            cost_limit_examples = train_schedule["cost_limit_examples"]
        else:
            cost_limit_examples = None
        
        max_batches = dataset_manager.getAvailableTrainBatches()
        if("batches_lim" in train_schedule):
            batch_lim = train_schedule["batches_lim"]
            if(batch_lim > max_batches):
                batches = max_batches
            else:
                batches = batch_lim
        else:
            batches = max_batches

        
        #inicializacion de varaibles requeridas
        print("==========Inicializando variables==================")
        self.session.run(init_vars)
        cost_log = open(self.name+"_cost_log_"+str(time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime()))+".txt", "w")
        cost_log.write("Name: "+self.name+"\n")
        cost_log.write("Epocs: "+str(epochs)+"\n")
        cost_log.write("Batches: "+str(batches)+"\n")
        cost_log.write("Learning Rate: "+str(learning_rate)+"\n")
        cost_log.write("Date: "+str(time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime()))+"\n")
        cost_log.write("train test \n")
        #begin training
        
        print("CUSTOM TRAINING")
        print("epocas: "+str(epochs))
        print("batches: "+str(batches))
        print("cost limit of examples: " +str(cost_limit_examples))
        print("name: " + self.name)

        #calculate train/test cost
        costt, costv = self.calculate_train_test_cost(dataset_manager, cost_limit_examples=cost_limit_examples)
        cost_log.write(str(costt)+","+str(costv)+"\n")
        
        dataset_manager.reset_indx()
        dataset_manager.shuffleTrain()
        max_local_valid = 7
    
        for e in range(0, epochs):
            print("Epoca: " +str(e))
            imgs_feed, flow_labels = dataset_manager.nextFlowBatch()
            for b in range(0, batches-1):
                #obten el sieguiente ejemplo de entrenamiento
                imgs_feed_next, flow_labels_next = dataset_manager.nextFlowBatch()

                #realiza paso de optimizacion
                prev_cost = self.session.run(self.cost, feed_dict={self.ph_X: imgs_feed_next, self.ph_Y: flow_labels_next, self.keep_prob: 1.0})
                self.session.run(train_op, feed_dict={self.ph_X: imgs_feed, self.ph_Y: flow_labels, self.keep_prob:self.dropout})
                next_cost = self.session.run(self.cost, feed_dict={self.ph_X: imgs_feed_next, self.ph_Y: flow_labels_next, self.keep_prob: 1.0})
                
                k = 1
                while(prev_cost < next_cost and k < max_local_valid):
                    prev_cost = next_cost
                    self.session.run(train_op, feed_dict={self.ph_X: imgs_feed, self.ph_Y: flow_labels, self.keep_prob:self.dropout})
                    next_cost = self.session.run(self.cost, feed_dict={self.ph_X: imgs_feed_next, self.ph_Y: flow_labels_next, self.keep_prob: 1.0})
                    k = k + 1
                
                imgs_feed = imgs_feed_next
                flow_labels = flow_labels_next 

                #muestra en consola el costo en cierto paso de batch
                if(b % batch_display == 0):
                    #obten el costo actualizado con los pesos
                    c = self.session.run(self.cost, feed_dict={self.ph_X: imgs_feed, self.ph_Y: flow_labels, self.keep_prob: 1.0})    
                    print("Epoca: "+str(e)+" | Paso de batch: "+ str(b) + " | Costo: " + str(c))
                    #cost_log.write(str(c)+"\n")
                
                #guarda el modelo cada cierto paso de entrenamiento
                if(b % save_batch_step == 0):
                    p = amod+"/"+"N"+str(self.stack)+"/naive_model.ckpt"
                    self.savers[-1].save(self.session, p)
                    print("Modelo guardado")
                
                #guarda en el archivo log el costo actual cada cierto paso de batch
                if(write_to_log_batch != None):
                    if(b % write_to_log_batch == 0):
                        costt, costv = self.calculate_train_test_cost(dataset_manager, cost_limit_examples=cost_limit_examples)
                        cost_log.write(str(costt)+","+str(costv)+"\n")
            
            dataset_manager.shuffleTrain()
            dataset_manager.reset_indx()
                
        p = amod+"/"+"N"+str(self.stack)+"/naive_model.ckpt"
        self.savers[-1].save(self.session, p)
        print("Modelo guardado")
        cost_log.close()
    
    #para usar unicamente en el entrenamiento
    def save_models_now(self):
        if(self.session == None):
            print("No se puede guardar porque no se ha iniciado sesion")
            return 
        
        if(self.trainable_list == None):
            p = self.amod+"/"+"N"+str(self.stack)+"/naive_model.ckpt"
            self.savers[-1].save(self.session, p)
            print("Ultimo Modelo Guardado")
        else:
            for i in range(self.stack):
                if(self.trainable_list[i]):
                    p = self.amod+"/"+"N"+str(i+1)+"/naive_model.ckpt"
                    self.savers[i].save(self.session, p)
                    print("Modelo "+str(i+1)+ " Guardado")


    #frame1 y frame2 iamgenes como numpy arrays      
    def inference(self, frame1, frame2):
        if(self.session == None):
            print("No se ha iniciado sesion")
            return

        pred = None
        if (self.models_loaded == True and self.init_last == False):
            input = np.concatenate([frame1, frame2], axis=2)
            input = np.reshape(input, [1, input.shape[0], input.shape[1], input.shape[2]])
            #no se porque pero la prediccion la devuelve con una dimension vacia de más
            pred = self.session.run([self.predic], feed_dict={self.ph_X: input, self.keep_prob: 1.0})
        else:
            print("Un modelo no ha sido cargado para predicción debidamente")

        return pred

    #frames lista de listas
    def inferenceMulti(self, frames):
        if(self.session == None):
            print("No se ha iniciado sesion")
            return None

        pred = []
        for frame12 in frames:
            pre = self.inference(frame12[0], frame12[1])
            pred.append(pre)
        else:
            print("Un modelo no ha sido cargado para predicción debidamente")

        return pred

    