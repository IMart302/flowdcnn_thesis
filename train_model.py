# coding=utf-8

import serial_flownet as snet
import flow_models_base as fmods 
import dataset_utils as fds
import os
import time

#windows
#dataset_path = "C:\LinuxNexum\Documents\FlyingChairs_release\data"
#linux server k40
dataset_path = "/home/ivan/Datasets/FlyingChairs_release/data"
#turriza 1080
#dataset_path = ""
cwd = os.getcwd()

lr_forall = 0.000001
examples = 400
fragments = 5
epocs = 50
today = time.strftime("%Y-%m-%d")
optimizer = "ADAM"

traink1 = [[1, 320]]
valk1 = [[321, 400]]

traink2 = [[1, 240], [321, 400]]
valk2 = [[241, 320]]

traink3 = [[1, 160], [241, 400]]
valk3 = [[161, 240]]

traink4 = [[1, 80], [161, 400]]
valk4 = [[81, 160]]

traink5 = [[81, 400]]
valk5 = [[1, 80]]

train1000 = [[1, 320], [401, 1080]]
valk1000 = [[321, 350]]

#restringido usar del 1122 al 1214

train1000 = [[1, 320], [401, 1080]]

train5000 = [[1, 320], [401, 1080], [2001, 6000]]

train10000 = [[1, 320], [401, 1080], [2001, 11000]]

############################################################################################
"""
name_model = "SNET_DEBUG_K1FROM10_500EX_"+fmods.FlowNetBrickModel.name_type
models_ckpt_paths = []
train_schedule = {"epochs":epocs, "valid_step":1, "optimizer":optimizer, "learning_rate": lr_forall}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET_DEBUG_K1FROM10_400EX_"+fmods.FlowNetBrickModel.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":epocs, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

#####################################################################################################
"""
name_model = "SNET_DEBUG_K1FROM10_500EX_"+fmods.NaiveFlowNetModel2.name_type
models_ckpt_paths = []
train_schedule = {"epochs":epocs, "valid_step":1, "optimizer":optimizer, "learning_rate": lr_forall}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

"""
name_model = "SNET_DEBUG_L1LOSS_"+fmods.NaiveFlowNetModel2.name_type
models_ckpt_paths = []
train_schedule = {"epochs":epocs, "valid_step":1, "optimizer":optimizer, "learning_rate": lr_forall}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

#################################################################################################
#################################################################################################

"""
name_model = "SNET_DEBUG_CHECK_400EX_"+fmods.FlowNetBrickModel.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.00001, "cost_log_write_batch": 25}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""
"""
name_model = "SNET_DEBUG_CHECK_400EX_"+fmods.FlowNetBrickModel.name_type+"_LR_0001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.0001, "cost_log_write_batch": 25}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""
"""
name_model = "SNET_DEBUG_CHECK_400EX_"+fmods.FlowNetBrickModel.name_type+"_LR_000001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.000001, "cost_log_write_batch": 25}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""
###############################################################################################

"""
name_model = "SNET_DEBUG_CHECK_400EX_"+fmods.NaiveFlowNetModel3.name_type+"_LR_000001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.000001, "cost_log_write_batch": 25}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel3()]
"""

"""
name_model = "SNET_DEBUG_CHECK_400EX_"+fmods.NaiveFlowNetModel3.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.00001, "cost_log_write_batch": 25}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel3()]
"""
"""
name_model = "SNET_DEBUG_CHECK_400EX_"+fmods.NaiveFlowNetModel3.name_type+"_LR_0001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.0001, "cost_log_write_batch": 25}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel3()]
"""

#########################################################################################################
"""
name_model = "SNET_DEBUG_CHECK_400EX_"+fmods.NaiveFlowNetModel2.name_type+"_LR_0001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.0001, "cost_log_write_batch": 25}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

############################################################################################################
"""
name_model = "SNET_DEBUG_L1LOSS_1000EX_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":5, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.00001, "cost_log_write_batch": 50, "cost_limit_examples": 40}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, train1000, valk1000, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""
"""
name_model = "SNET_DEBUG_L1LOSS_1000EX_"+fmods.FlowNetBrickModel3.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":5, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.00001, "cost_log_write_batch": 50, "cost_limit_examples": 40}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, train1000, valk1000, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel3()]
"""
""""
name_model = "SNET_DEBUG_L1LOSS_CUSTOMTRAIN_1000EX_"+fmods.FlowNetBrickModel2.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":5, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.00001, "cost_log_write_batch": 50, "cost_limit_examples": 40}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, train1000, valk1000, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""

##################################################################################################################
"""
name_model = "SNET_DEBUG_L1LOSS_1000EX_"+fmods.FlowNetBrickModel4.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":5, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.00001, "cost_log_write_batch": 50, "cost_limit_examples": 40}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, train1000, valk1000, 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel4()]
"""
###########################################################################################################

"""
name_model = "SNET_Adadelta_L1LOSS_2000EX_B25_"+fmods.FlowNetBrickModel5.name_type+"_LR_001"
models_ckpt_paths = []
train_schedule = {"epochs":5, "valid_step":1, "optimizer":"Adadelta", "learning_rate": 0.001, "cost_log_write_batch": 2, "cost_limit_examples": 2}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, train2000, valk1000, 25, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SNET_Adam_L1LOSS_5000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":"ADAM", "learning_rate": 0.00001, "cost_log_write_batch": 2, "cost_limit_examples": 2}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, train5000, valk1000, 20, shuffle=False, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""

"""
name_model = "SNETNORM_Adam_L1LOSS_5000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_0001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":"ADAM", "learning_rate": 0.0001, "cost_log_write_batch": 2, "cost_limit_examples": 2}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, train5000, valk1000, 20, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SNETNORM_Adam_L1LOSS_22000EX_B10_"+fmods.FlowNetBrickModel5.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":10, "valid_step":1, "optimizer":"ADAM", "learning_rate": 0.00001, "cost_log_write_batch": 10000, "cost_limit_examples": 10}
save_schedule = {"save_epoch":1, "save_batch_step":1000}
train, test = fds.ChairsDatasetManager.trainTestSplitToListRange(cwd+"/"+"FlyingChairs_release_test_train_split.list", limit=18000)
chairs_ds = fds.ChairsDatasetManager(dataset_path, train, test, 10, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5()]
"""
"""
name_model = "SNETNORM_Adam_L1LOSS_5000EX_B10_"+fmods.FlowNetBrickModel7.name_type+"_LR_0001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":"ADAM", "learning_rate": 0.0001, "cost_log_write_batch": 2, "cost_limit_examples": 2}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, train5000, valk1000, 10, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel7()]
"""
"""
name_model = "SNETNORM_Adam_L1LOSS_5000EX_B20_"+fmods.FlowNetBrickModel2.name_type+"_LR_0001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":"ADAM", "learning_rate": 0.0001, "cost_log_write_batch": 2, "cost_limit_examples": 2}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, train5000, valk1000, 20, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel2()]
"""

"""
name_model = "SNETNORM_Adam_L1LOSS_400EX_B1_"+fmods.FlowNetBrickModel7.name_type+"_LR_0001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.0001, "cost_log_write_batch": 25}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel7()]
"""

name_model = "SNETNORM_Adam_L1LOSS_400EX_B1_"+fmods.DilatFlowNet3.name_type+"_LR_00001"
models_ckpt_paths = []
train_schedule = {"epochs":7, "valid_step":1, "optimizer":optimizer, "learning_rate": 0.00001, "cost_log_write_batch": 25}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, traink1, valk1, 1, shuffle=False, load_in_ram=True, normalize=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.DilatFlowNet3()]

shape = chairs_ds.getFinalShape() #height, width
mflownet.model_build(shape[0], shape[1], mods, last_trainable=True)
mflownet.init_session()
mflownet.model_session_chkpt_restore(models_ckpt_paths)
#mflownet.train(chairs_ds, train_schedule, save_schedule)
mflownet.train2(chairs_ds, train_schedule, save_schedule) #fix gpu lack of memory
mflownet.close_session()