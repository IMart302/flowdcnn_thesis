# coding=utf-8

import tensorflow as tf
import numpy as np
import conv_utils as conv
import flow_metrics as fmetric

class FlowNetModelBase(object):
    name_type = "BASE"
    def __init__(self):
        self.require_keep_prob = False
        self.layers = None 
        self.output = None
        self.global_step = None
        self.model_scope_build = None

    #override this method
    #put keep prob only if required
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        raise NotImplementedError
    
    def getLayers(self):
        return self.layers
    
    def getOutputTensor(self):
        return self.output

    def getAllVars(self):
        layers = self.layers.values()
        varss = []
        for l in layers:
            varss.append(l.getWeights())
            varss.append(l.getBiases())
        
        return varss
    
    def getTrainVars(self):
        train_vars = tf.trainable_vars(scope=self.model_scope_build)
        return train_vars
    
    def get_local_step(self):
        return self.global_step

    def makeLoss(self, target_tensor, flow_loss="L1", add_regularized_vars=False):
        if(flow_loss=="L2"):
            loss_aux = fmetric.l2_error(self.output, target_tensor)
        elif(flow_loss=="AEPE"):
            loss_aux = fmetric.average_endpoint_error(self.output, target_tensor)
        else:
            loss_aux = fmetric.l1_error(self.output, target_tensor)

        if(add_regularized_vars):
            total_loss = tf.losses.get_total_loss(add_regularization_losses=True)
        else:
            total_loss = tf.losses.get_total_loss(add_regularization_losses=False)

        return total_loss

class NaiveFlowNetModel2(FlowNetModelBase):
    name_type = "NM2"
    def __init__(self):
        self.layers = {}
  
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        self.model_scope_build=model_scope
        with tf.variable_scope(model_scope):
            #Define una capa de convolucion 60 filtros de 5x5x3 (como la entrada es te 3 canales) y activacion relu
            #convolucion en paralelo con distintas dilataciones
            #primera convolucion (1) 60 filtros de 5x5x3 activacion elu, padding=SAME
            l1_1 = conv.Conv2DLayer(input, "l1_1", [5, 5, input_channels], 60, activation="RELU", trainable=trainable)

            #primera convolucion (2) 60 filtros de 5x5x3 activacion elu dilatacion de 2, padding=SAME
            l1_2 = conv.Conv2DLayer(input, "l1_2", [5, 5, input_channels], 60, dilation=2, activation="RELU", trainable=trainable)

            #primera convolucion (3) 60 filtros de 5x5x3 activacion elu dilatacion de 3, padding=SAME
            l1_3 = conv.Conv2DLayer(input, "l1_3", [5, 5, input_channels], 60, dilation=3, activation="RELU", trainable=trainable)

            #concatenacion de los features maps de las conovoluciones en paralelo

             #60 + 60 + 60 = 180 mapas de caracteristicas, misma dimension que la entrada en width y height
            concat1 = tf.concat([l1_1.getOutputTensor(), l1_2.getOutputTensor(), l1_3.getOutputTensor()], axis=3, name="concat1")
  
            #===Parte contractiva=======

            #convolucion 2, 60 + 60 + 60 salidas = 180 mapas de entrada del mismo width y height del input, padding VALID, activacion elu
            l2 = conv.Conv2DLayer(concat1, "l2", [5, 5, 180], 256, activation="RELU", padding="VALID", trainable=trainable)

            #convolucion 3, 256 mapas de entrada, 512 filtros de 5x5x256, activacion elu
            l3 = conv.Conv2DLayer(l2.getOutputTensor(), "l3", [5, 5, 256], 256, activation="RELU", padding="VALID", trainable=trainable)
            
            #convolucion 4, 256 mapas de entrada, 512 filtros de 5x5x256, activacion elu
            l4 = conv.Conv2DLayer(l3.getOutputTensor(), "l4", [5, 5, 256], 512, activation="RELU", padding="VALID", trainable=trainable)

            #===Aumento de resolucion=====

        
            ul1 = conv.TransposedConv2DLayer(l4.getOutputTensor(), "ul1", [5, 5, 512], 120, padding="VALID", trainable=trainable)
            #convolucion transpuesta 2, 2 filtros de 5x5x2, regreso a resolucion de input pero en 2 canales (mismos del flow)
            
            ul2 = conv.TransposedConv2DLayer(ul1.getOutputTensor(), "ul2", [5, 5, 120], 60, padding="VALID", trainable=trainable)
            
            ul3 = conv.TransposedConv2DLayer(ul2.getOutputTensor(), "ul3", [5, 5, 60], 2, padding="VALID", trainable=trainable)

            self.global_step = tf.get_variable("global_step", shape=[], dtype=tf.int64, initializer=tf.zeros_initializer())

        self.output = ul3.getOutputTensor()
        #ul1.forgetOutput()
        #ul2.forgetOutput()

        self.layers = {"l1_1": l1_1, "l1_2": l1_2, "l1_3": l1_3, "l2": l2, "l3": l3, "l4": l4, "ul1": ul1, "ul2": ul2, "ul3": ul3}


class NaiveFlowNetModel3(FlowNetModelBase):
    name_type = "NM3"
    def __init__(self):
        pass
    
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        #prim_width = args["input_width"]
        #prim_height = args["input_height"]
        self.model_scope_build=model_scope
        #static shapes
        prim_width = int(input.shape[1])
        print(prim_width)
        prim_height = int(input.shape[2])
        print(prim_height)

        with tf.variable_scope(model_scope):
            self.layers = {}
            l1 = conv.Conv2DLayer(input, "l1", [5, 5, input_channels], 120, activation=True, trainable=trainable, padding="VALID")
            self.layers["l1"] = l1
            nw = (prim_width-4)
            nh = (prim_height-4)
            l2 = conv.Conv2DLayer(l1.getOutputTensor(), "l2", [5, 5, 120], 240, activation=True, trainable=trainable, padding="VALID")
            self.layers["l2"] = l2
            nw = (nw - 4)
            nh = (nh - 4)
            reshape_in = tf.image.resize_images(input, (nw, nh))

            conc1 = tf.concat([reshape_in, l2.getOutputTensor()], axis=3, name="concat1")

            l3 = conv.Conv2DLayer(conc1, "l3", [5, 5, 240+input_channels], 420, activation=True, trainable=trainable, padding="VALID")
            self.layers["l3"] = l3
            nw = (nw - 4)
            nh = (nh - 4)
            l4 = conv.Conv2DLayer(l3.getOutputTensor(), "l4", [5, 5, 420], 512, activation=True, trainable=trainable, padding="VALID")
            self.layers["l4"] = l4
            nw = (nw - 4)
            nh = (nh - 4)

            reshape_in = tf.image.resize_images(input, (nw, nh))

            conc2 = tf.concat([reshape_in, l4.getOutputTensor()], axis=3, name="concat2")
            
            ul1 = conv.TransposedConv2DLayer(conc2, "ul1", [5, 5, 512+input_channels], 30, padding="VALID", trainable=trainable)
            self.layers["ul1"] = ul1
            nw = (nw + 4)
            nh = (nh + 4)
            pred_l2 = conv.Conv2DLayer(ul1.getOutputTensor(), "pred_l2", [5, 5, 30], 2, trainable=trainable, padding="VALID")
            self.layers["pred_l2"] = pred_l2
            pred_w = (nw - 4)            

            pad = int((nw - pred_w)/2)  
            pred_l2_pad = tf.pad(pred_l2.getOutputTensor(), [[0, 0], [pad, pad], [pad, pad], [0, 0]])
            
            conc3 = tf.concat([pred_l2_pad, ul1.getOutputTensor()], axis=3, name="concat3")
            ul2 = conv.TransposedConv2DLayer(conc3, "ul2", [5, 5, 30+2], 60, padding="VALID", trainable=trainable)
            self.layers["ul2"] = ul2
            nw = (nw + 4)
            nh = (nh + 4)

            ul3 = conv.TransposedConv2DLayer(ul2.getOutputTensor(), "ul3", [5, 5, 60], 60, padding="VALID", trainable=trainable)
            self.layers["ul3"] = ul3
            nw = (nw + 4)
            nh = (nh + 4)

            pred_l1 = conv.Conv2DLayer(ul3.getOutputTensor(), "pred_l1", [5, 5, 60], 2, trainable=trainable, padding="VALID")
            self.layers["pred_l1"] = pred_l1
            pred_w = (nw - 4)

            pad = int((nw - pred_w)/2)
            pred_l1_pad = tf.pad(pred_l1.getOutputTensor(), [[0, 0], [pad, pad], [pad, pad], [0, 0]])

            conc4 = tf.concat([pred_l1_pad, ul3.getOutputTensor()], axis=3, name="concat4")
            
            ul4 = conv.TransposedConv2DLayer(conc4, "ul4", [5, 5, 60+2], 80, padding="VALID", trainable=trainable)
            self.layers["ul4"] = ul4

            ul5 = conv.TransposedConv2DLayer(ul4.getOutputTensor(), "ul5", [5, 5, 80], 80, padding="VALID", trainable=trainable)
            self.layers["ul5"] = ul5
            pred_l0 = conv.Conv2DLayer(ul5.getOutputTensor(), "pred_l0", [5, 5, 80], 2, trainable=trainable, padding="VALID")
            self.layers["pred_l0"] = pred_l0
            
            self.output = pred_l0.getOutputTensor()
            self.global_step = tf.get_variable("global_step", shape=[], dtype=tf.int64, initializer=tf.zeros_initializer())


    def makeLoss(self, target_tensor):
        pred = self.layers["pred_l0"].getOutputTensor()
        loss_0 = fmetric.average_endpoint_error(pred, target_tensor)

        new_width = int(target_tensor.shape[1]) - 8
        new_height = int(target_tensor.shape[2]) - 8
        reshape_target = tf.image.resize_images(target_tensor, (new_width, new_height))
        
        #scale factor correction
        #f = float(width)/float(int(target_tensor.shape[1]))
        f = float(int(target_tensor.shape[1]))/float(new_width)
        reshape_target = (1.0/f)*reshape_target
        
        pred = self.layers["pred_l1"].getOutputTensor()
        loss_1 = fmetric.average_endpoint_error(pred, reshape_target)

        new_width = int(target_tensor.shape[1]) - 16
        new_height = int(target_tensor.shape[2]) - 16
        reshape_target = tf.image.resize_images(target_tensor, (new_width, new_height))
        
        #scale factor correction
        f = float(int(target_tensor.shape[1]))/float(new_width)
        reshape_target = (1.0/f)*reshape_target
        
        pred = self.layers["pred_l2"].getOutputTensor()
        loss_2 = fmetric.average_endpoint_error(pred, reshape_target)
        w = 0.142857142857143
 
        total_loss = tf.losses.compute_weighted_loss([loss_0, loss_1, loss_2], [4.0*w, 2.0*w, w])
        #total_loss = 4.0*w*loss_0 + 2.0*w*loss_1 + w*loss_2
        return total_loss


class FlowNetBrickModel(FlowNetModelBase):
    name_type ="Brick1"
    def __init__(self):
        self.layers = {}
        pass
    
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        self.model_scope_build=model_scope
        iwidth = int(input.shape[1])
        iheight = int(input.shape[2])
        with tf.variable_scope(model_scope):
            conv11 = conv.Conv2DLayer(input, "conv11", [7, 7, input_channels], 64, padding="VALID", activation="RELU", trainable=trainable)
            conv12 = conv.Conv2DLayer(conv11.getOutputTensor(), "conv12", [5, 5, 64], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv13 = conv.Conv2DLayer(conv12.getOutputTensor(), "conv13", [5, 5, 120], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv14 = conv.Conv2DLayer(conv13.getOutputTensor(), "conv14", [5, 5, 120], 240, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv11"] = conv11
            self.layers["conv12"] = conv12
            self.layers["conv13"] = conv13
            self.layers["conv14"] = conv14

            reduc = 18
            resh = tf.image.resize_images(input, (iwidth - reduc, iheight - reduc))
            concat1 = tf.concat([resh, conv14.getOutputTensor()], axis=3)

            conv21 = conv.Conv2DLayer(concat1, "conv21", [7, 7, 240+input_channels], 60, padding="VALID", activation="RELU", trainable=trainable)
            conv22 = conv.Conv2DLayer(conv21.getOutputTensor(), "conv22", [5, 5, 60], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv23 = conv.Conv2DLayer(conv22.getOutputTensor(), "conv23", [5, 5, 120], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv24 = conv.Conv2DLayer(conv23.getOutputTensor(), "conv24", [5, 5, 120], 240, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv21"] = conv21
            self.layers["conv22"] = conv22
            self.layers["conv23"] = conv23
            self.layers["conv24"] = conv24

            reduc = 36
            resh = tf.image.resize_images(resh, (iwidth - reduc, iheight - reduc))
            concat2 = tf.concat([resh, conv24.getOutputTensor()], axis=3)

            conv31 = conv.Conv2DLayer(concat2, "conv31", [7, 7, 240+input_channels], 60, padding="VALID", activation="RELU", trainable=trainable)
            conv32 = conv.Conv2DLayer(conv31.getOutputTensor(), "conv32", [5, 5, 60], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv33 = conv.Conv2DLayer(conv32.getOutputTensor(), "conv33", [5, 5, 120], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv34 = conv.Conv2DLayer(conv33.getOutputTensor(), "conv34", [5, 5, 120], 240, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv31"] = conv31
            self.layers["conv32"] = conv32
            self.layers["conv33"] = conv33
            self.layers["conv34"] = conv34

            upconv31 = conv.TransposedConv2DLayer(conv34.getOutputTensor(), "upconv31", [5, 5, 240], 60, padding="VALID", trainable=trainable)
            upconv32 = conv.TransposedConv2DLayer(upconv31.getOutputTensor(), "upconv32", [5, 5, 60], 50, padding="VALID", trainable=trainable)
            upconv33 = conv.TransposedConv2DLayer(upconv32.getOutputTensor(), "upconv33", [5, 5, 50], 40, padding="VALID", trainable=trainable)
            upconv34 = conv.TransposedConv2DLayer(upconv33.getOutputTensor(), "upconv34", [7, 7, 40], 30, padding="VALID", trainable=trainable)
            self.layers["upconv31"] = upconv31
            self.layers["upconv32"] = upconv32
            self.layers["upconv33"] = upconv33
            self.layers["upconv34"] = upconv34

            concat3 = tf.concat([upconv34.getOutputTensor(), conv24.getOutputTensor()], axis=3)

            upconv21 = conv.TransposedConv2DLayer(concat3, "upconv21", [5, 5, 240+30], 50, padding="VALID", trainable=trainable)
            upconv22 = conv.TransposedConv2DLayer(upconv21.getOutputTensor(), "upconv22", [5, 5, 50], 40, padding="VALID", trainable=trainable)
            upconv23 = conv.TransposedConv2DLayer(upconv22.getOutputTensor(), "upconv23", [5, 5, 40], 30, padding="VALID", trainable=trainable)
            upconv24 = conv.TransposedConv2DLayer(upconv23.getOutputTensor(), "upconv24", [7, 7, 30], 20, padding="VALID", trainable=trainable)
            self.layers["upconv21"] = upconv21
            self.layers["upconv22"] = upconv22
            self.layers["upconv23"] = upconv23
            self.layers["upconv24"] = upconv24

            concat4 = tf.concat([upconv24.getOutputTensor(), conv14.getOutputTensor()], axis=3)

            upconv11 = conv.TransposedConv2DLayer(concat4, "upconv11", [5, 5, 240+20], 40, padding="VALID", trainable=trainable)
            upconv12 = conv.TransposedConv2DLayer(upconv11.getOutputTensor(), "upconv12", [5, 5, 40], 30, padding="VALID", trainable=trainable)
            upconv13 = conv.TransposedConv2DLayer(upconv12.getOutputTensor(), "upconv13", [5, 5, 30], 20, padding="VALID", trainable=trainable)
            upconv14 = conv.TransposedConv2DLayer(upconv13.getOutputTensor(), "upconv14", [7, 7, 20], 10, padding="VALID", trainable=trainable)
            self.layers["upconv11"] = upconv11
            self.layers["upconv12"] = upconv12
            self.layers["upconv13"] = upconv13
            self.layers["upconv14"] = upconv14

            prepad = tf.pad(upconv14.getOutputTensor(), [[0, 0], [2, 2], [2, 2], [0, 0]])
            predict = conv.Conv2DLayer(prepad, "predict", [5, 5, 10], 2, padding="VALID", trainable=trainable)
            self.layers["predict"] = predict
            self.output=predict.getOutputTensor()
            self.global_step = tf.get_variable("global_step", shape=[], dtype=tf.int64, initializer=tf.zeros_initializer())




class FlowNetBrickModel2(FlowNetModelBase):
    name_type ="DUNET"
    def __init__(self):
        self.layers = {}
        return
    
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        self.model_scope_build=model_scope
        with tf.variable_scope(model_scope):
            conv11 = conv.Conv2DLayer(input, "conv11", [7, 7, input_channels], 64, padding="VALID", activation="RELU", trainable=trainable)
            conv12 = conv.Conv2DLayer(conv11.getOutputTensor(), "conv12", [5, 5, 64], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv13 = conv.Conv2DLayer(conv12.getOutputTensor(), "conv13", [5, 5, 120], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv14 = conv.Conv2DLayer(conv13.getOutputTensor(), "conv14", [5, 5, 120], 240, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv11"] = conv11
            self.layers["conv12"] = conv12
            self.layers["conv13"] = conv13
            self.layers["conv14"] = conv14


            conv21 = conv.Conv2DLayer(conv14.getOutputTensor(), "conv21", [7, 7, 240], 60, padding="VALID", activation="RELU", trainable=trainable)
            conv22 = conv.Conv2DLayer(conv21.getOutputTensor(), "conv22", [5, 5, 60], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv23 = conv.Conv2DLayer(conv22.getOutputTensor(), "conv23", [5, 5, 120], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv24 = conv.Conv2DLayer(conv23.getOutputTensor(), "conv24", [5, 5, 120], 240, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv21"] = conv21
            self.layers["conv22"] = conv22
            self.layers["conv23"] = conv23
            self.layers["conv24"] = conv24


            conv31 = conv.Conv2DLayer(conv24.getOutputTensor(), "conv31", [7, 7, 240], 60, padding="VALID", activation="RELU", trainable=trainable)
            conv32 = conv.Conv2DLayer(conv31.getOutputTensor(), "conv32", [5, 5, 60], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv33 = conv.Conv2DLayer(conv32.getOutputTensor(), "conv33", [5, 5, 120], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv34 = conv.Conv2DLayer(conv33.getOutputTensor(), "conv34", [5, 5, 120], 240, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv31"] = conv31
            self.layers["conv32"] = conv32
            self.layers["conv33"] = conv33
            self.layers["conv34"] = conv34

            upconv31 = conv.TransposedConv2DLayer(conv34.getOutputTensor(), "upconv31", [5, 5, 240], 60, padding="VALID", trainable=trainable)
            upconv32 = conv.TransposedConv2DLayer(upconv31.getOutputTensor(), "upconv32", [5, 5, 60], 50, padding="VALID", trainable=trainable)
            upconv33 = conv.TransposedConv2DLayer(upconv32.getOutputTensor(), "upconv33", [5, 5, 50], 40, padding="VALID", trainable=trainable)
            upconv34 = conv.TransposedConv2DLayer(upconv33.getOutputTensor(), "upconv34", [7, 7, 40], 30, padding="VALID", trainable=trainable)
            self.layers["upconv31"] = upconv31
            self.layers["upconv32"] = upconv32
            self.layers["upconv33"] = upconv33
            self.layers["upconv34"] = upconv34

            concat3 = tf.concat([upconv34.getOutputTensor(), conv24.getOutputTensor()], axis=3)

            upconv21 = conv.TransposedConv2DLayer(concat3, "upconv21", [5, 5, 240+30], 50, padding="VALID", trainable=trainable)
            upconv22 = conv.TransposedConv2DLayer(upconv21.getOutputTensor(), "upconv22", [5, 5, 50], 40, padding="VALID", trainable=trainable)
            upconv23 = conv.TransposedConv2DLayer(upconv22.getOutputTensor(), "upconv23", [5, 5, 40], 30, padding="VALID", trainable=trainable)
            upconv24 = conv.TransposedConv2DLayer(upconv23.getOutputTensor(), "upconv24", [7, 7, 30], 20, padding="VALID", trainable=trainable)
            self.layers["upconv21"] = upconv21
            self.layers["upconv22"] = upconv22
            self.layers["upconv23"] = upconv23
            self.layers["upconv24"] = upconv24

            concat4 = tf.concat([upconv24.getOutputTensor(), conv14.getOutputTensor()], axis=3)

            upconv11 = conv.TransposedConv2DLayer(concat4, "upconv11", [5, 5, 240+20], 40, padding="VALID", trainable=trainable)
            upconv12 = conv.TransposedConv2DLayer(upconv11.getOutputTensor(), "upconv12", [5, 5, 40], 30, padding="VALID", trainable=trainable)
            upconv13 = conv.TransposedConv2DLayer(upconv12.getOutputTensor(), "upconv13", [5, 5, 30], 20, padding="VALID", trainable=trainable)
            upconv14 = conv.TransposedConv2DLayer(upconv13.getOutputTensor(), "upconv14", [7, 7, 20], 10, padding="VALID", trainable=trainable)
            self.layers["upconv11"] = upconv11
            self.layers["upconv12"] = upconv12
            self.layers["upconv13"] = upconv13
            self.layers["upconv14"] = upconv14

            prepad = tf.pad(upconv14.getOutputTensor(), [[0, 0], [3, 3], [3, 3], [0, 0]])
            predict = conv.Conv2DLayer(prepad, "predict", [7, 7, 10], 2, padding="VALID", trainable=trainable)
            self.layers["predict"] = predict
            self.output=predict.getOutputTensor()
            self.global_step = tf.get_variable("global_step", shape=[], dtype=tf.int64, initializer=tf.zeros_initializer(), trainable=False)



class FlowNetBrickModel3(FlowNetModelBase):
    name_type ="DUNET_DROP"
    def __init__(self):
        self.require_keep_prob = True
        self.layers = {}
        return
    
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        self.model_scope_build=model_scope
        with tf.variable_scope(model_scope):
            #rate = 1.0 - keep_prob
            conv11 = conv.Conv2DLayer(input, "conv11", [7, 7, input_channels], 64, padding="VALID", activation="RELU", trainable=trainable)
            conv12 = conv.Conv2DLayer(conv11.getOutputTensor(), "conv12", [5, 5, 64], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv12_drop = tf.nn.dropout(conv12.getOutputTensor(), keep_prob = keep_prob)
            conv13 = conv.Conv2DLayer(conv12_drop, "conv13", [5, 5, 120], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv14 = conv.Conv2DLayer(conv13.getOutputTensor(), "conv14", [5, 5, 120], 240, padding="VALID", activation="RELU", trainable=trainable)
            conv14_drop = tf.nn.dropout(conv14.getOutputTensor(), keep_prob = keep_prob)
            self.layers["conv11"] = conv11
            self.layers["conv12"] = conv12
            self.layers["conv13"] = conv13
            self.layers["conv14"] = conv14


            conv21 = conv.Conv2DLayer(conv14_drop, "conv21", [7, 7, 240], 60, padding="VALID", activation="RELU", trainable=trainable)
            conv22 = conv.Conv2DLayer(conv21.getOutputTensor(), "conv22", [5, 5, 60], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv22_drop = tf.nn.dropout(conv22.getOutputTensor(), keep_prob = keep_prob)
            conv23 = conv.Conv2DLayer(conv22_drop, "conv23", [5, 5, 120], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv24 = conv.Conv2DLayer(conv23.getOutputTensor(), "conv24", [5, 5, 120], 240, padding="VALID", activation="RELU", trainable=trainable)
            conv24_drop = tf.nn.dropout(conv24.getOutputTensor(), keep_prob = keep_prob)
            self.layers["conv21"] = conv21
            self.layers["conv22"] = conv22
            self.layers["conv23"] = conv23
            self.layers["conv24"] = conv24


            conv31 = conv.Conv2DLayer(conv24_drop, "conv31", [7, 7, 240], 60, padding="VALID", activation="RELU", trainable=trainable)
            conv32 = conv.Conv2DLayer(conv31.getOutputTensor(), "conv32", [5, 5, 60], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv32_drop = tf.nn.dropout(conv32.getOutputTensor(), keep_prob = keep_prob)
            conv33 = conv.Conv2DLayer(conv32_drop, "conv33", [5, 5, 120], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv34 = conv.Conv2DLayer(conv33.getOutputTensor(), "conv34", [5, 5, 120], 240, padding="VALID", activation="RELU", trainable=trainable)
            conv34_drop = tf.nn.dropout(conv34.getOutputTensor(), keep_prob = keep_prob)
            self.layers["conv31"] = conv31
            self.layers["conv32"] = conv32
            self.layers["conv33"] = conv33
            self.layers["conv34"] = conv34

            upconv31 = conv.TransposedConv2DLayer(conv34_drop, "upconv31", [5, 5, 240], 60, padding="VALID", trainable=trainable)
            upconv32 = conv.TransposedConv2DLayer(upconv31.getOutputTensor(), "upconv32", [5, 5, 60], 50, padding="VALID", trainable=trainable)
            upconv33 = conv.TransposedConv2DLayer(upconv32.getOutputTensor(), "upconv33", [5, 5, 50], 40, padding="VALID", trainable=trainable)
            upconv34 = conv.TransposedConv2DLayer(upconv33.getOutputTensor(), "upconv34", [7, 7, 40], 30, padding="VALID", trainable=trainable)
            self.layers["upconv31"] = upconv31
            self.layers["upconv32"] = upconv32
            self.layers["upconv33"] = upconv33
            self.layers["upconv34"] = upconv34

            concat3 = tf.concat([upconv34.getOutputTensor(), conv24.getOutputTensor()], axis=3)

            upconv21 = conv.TransposedConv2DLayer(concat3, "upconv21", [5, 5, 240+30], 50, padding="VALID", trainable=trainable)
            upconv22 = conv.TransposedConv2DLayer(upconv21.getOutputTensor(), "upconv22", [5, 5, 50], 40, padding="VALID", trainable=trainable)
            upconv23 = conv.TransposedConv2DLayer(upconv22.getOutputTensor(), "upconv23", [5, 5, 40], 30, padding="VALID", trainable=trainable)
            upconv24 = conv.TransposedConv2DLayer(upconv23.getOutputTensor(), "upconv24", [7, 7, 30], 20, padding="VALID", trainable=trainable)
            self.layers["upconv21"] = upconv21
            self.layers["upconv22"] = upconv22
            self.layers["upconv23"] = upconv23
            self.layers["upconv24"] = upconv24

            concat4 = tf.concat([upconv24.getOutputTensor(), conv14.getOutputTensor()], axis=3)

            upconv11 = conv.TransposedConv2DLayer(concat4, "upconv11", [5, 5, 240+20], 40, padding="VALID", trainable=trainable)
            upconv12 = conv.TransposedConv2DLayer(upconv11.getOutputTensor(), "upconv12", [5, 5, 40], 30, padding="VALID", trainable=trainable)
            upconv13 = conv.TransposedConv2DLayer(upconv12.getOutputTensor(), "upconv13", [5, 5, 30], 20, padding="VALID", trainable=trainable)
            upconv14 = conv.TransposedConv2DLayer(upconv13.getOutputTensor(), "upconv14", [7, 7, 20], 10, padding="VALID", trainable=trainable)
            self.layers["upconv11"] = upconv11
            self.layers["upconv12"] = upconv12
            self.layers["upconv13"] = upconv13
            self.layers["upconv14"] = upconv14

            prepad = tf.pad(upconv14.getOutputTensor(), [[0, 0], [3, 3], [3, 3], [0, 0]])
            predict = conv.Conv2DLayer(prepad, "predict", [7, 7, 10], 2, padding="VALID", trainable=trainable)
            self.layers["predict"] = predict
            self.output=predict.getOutputTensor()
            self.global_step = tf.get_variable("global_step", shape=[], dtype=tf.int64, initializer=tf.zeros_initializer())


class FlowNetBrickModel4(FlowNetModelBase):
    name_type ="DUNET_DILAT"
    def __init__(self):
        self.layers = {}
        return
    
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        self.model_scope_build=model_scope
        with tf.variable_scope(model_scope):
            conv11 = conv.Conv2DLayer(input, "conv11", [7, 7, input_channels], 80, padding="VALID", activation="RELU", trainable=trainable)
            conv12 = conv.Conv2DLayer(conv11.getOutputTensor(), "conv12", [5, 5, 80], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv13 = conv.Conv2DLayer(conv12.getOutputTensor(), "conv13", [5, 5, 120], 160, padding="VALID", activation="RELU", trainable=trainable)
            conv14 = conv.Conv2DLayer(conv13.getOutputTensor(), "conv14", [5, 5, 160], 200, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv11"] = conv11
            self.layers["conv12"] = conv12
            self.layers["conv13"] = conv13
            self.layers["conv14"] = conv14

            convdil3 = conv.Conv2DLayer(input, "convdil3", [7, 7, input_channels], 80, dilation=3, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["convdil3"] = convdil3
            conc11 = tf.concat([convdil3.getOutputTensor(), conv14.getOutputTensor()], axis=3)

            conv21 = conv.Conv2DLayer(conc11, "conv21", [7, 7, 200+80], 240, padding="VALID", activation="RELU", trainable=trainable)
            conv22 = conv.Conv2DLayer(conv21.getOutputTensor(), "conv22", [5, 5, 240], 280, padding="VALID", activation="RELU", trainable=trainable)
            conv23 = conv.Conv2DLayer(conv22.getOutputTensor(), "conv23", [5, 5, 280], 320, padding="VALID", activation="RELU", trainable=trainable)
            conv24 = conv.Conv2DLayer(conv23.getOutputTensor(), "conv24", [5, 5, 320], 360, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv21"] = conv21
            self.layers["conv22"] = conv22
            self.layers["conv23"] = conv23
            self.layers["conv24"] = conv24
            
            convdil6 = conv.Conv2DLayer(input, "convdil6", [7, 7, input_channels], 80, dilation=6, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["convdil6"] = convdil6
            conc12 = tf.concat([convdil6.getOutputTensor(), conv24.getOutputTensor()], axis=3)

            conv31 = conv.Conv2DLayer(conc12, "conv31", [7, 7, 360+80], 400, padding="VALID", activation="RELU", trainable=trainable)
            conv32 = conv.Conv2DLayer(conv31.getOutputTensor(), "conv32", [5, 5, 400], 440, padding="VALID", activation="RELU", trainable=trainable)
            conv33 = conv.Conv2DLayer(conv32.getOutputTensor(), "conv33", [5, 5, 440], 480, padding="VALID", activation="RELU", trainable=trainable)
            conv34 = conv.Conv2DLayer(conv33.getOutputTensor(), "conv34", [5, 5, 480], 520, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv31"] = conv31
            self.layers["conv32"] = conv32
            self.layers["conv33"] = conv33
            self.layers["conv34"] = conv34

            convdil9 = conv.Conv2DLayer(input, "convdil9", [7, 7, input_channels], 80, dilation=9, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["convdil9"] = convdil9
            conc13 = tf.concat([convdil9.getOutputTensor(), conv34.getOutputTensor()], axis=3)

            upconv31 = conv.TransposedConv2DLayer(conc13, "upconv31", [5, 5, 520+80], 120, padding="VALID", trainable=trainable)
            upconv32 = conv.TransposedConv2DLayer(upconv31.getOutputTensor(), "upconv32", [5, 5, 120], 110, padding="VALID", trainable=trainable)
            upconv33 = conv.TransposedConv2DLayer(upconv32.getOutputTensor(), "upconv33", [5, 5, 110], 100, padding="VALID", trainable=trainable)
            upconv34 = conv.TransposedConv2DLayer(upconv33.getOutputTensor(), "upconv34", [7, 7, 100], 90, padding="VALID", trainable=trainable)
            self.layers["upconv31"] = upconv31
            self.layers["upconv32"] = upconv32
            self.layers["upconv33"] = upconv33
            self.layers["upconv34"] = upconv34

            concat3 = tf.concat([upconv34.getOutputTensor(), conv24.getOutputTensor()], axis=3)

            upconv21 = conv.TransposedConv2DLayer(concat3, "upconv21", [5, 5, 360+90], 80, padding="VALID", trainable=trainable)
            upconv22 = conv.TransposedConv2DLayer(upconv21.getOutputTensor(), "upconv22", [5, 5, 80], 70, padding="VALID", trainable=trainable)
            upconv23 = conv.TransposedConv2DLayer(upconv22.getOutputTensor(), "upconv23", [5, 5, 70], 60, padding="VALID", trainable=trainable)
            upconv24 = conv.TransposedConv2DLayer(upconv23.getOutputTensor(), "upconv24", [7, 7, 60], 50, padding="VALID", trainable=trainable)
            self.layers["upconv21"] = upconv21
            self.layers["upconv22"] = upconv22
            self.layers["upconv23"] = upconv23
            self.layers["upconv24"] = upconv24

            concat4 = tf.concat([upconv24.getOutputTensor(), conv14.getOutputTensor()], axis=3)

            upconv11 = conv.TransposedConv2DLayer(concat4, "upconv11", [5, 5, 200+50], 40, padding="VALID", trainable=trainable)
            upconv12 = conv.TransposedConv2DLayer(upconv11.getOutputTensor(), "upconv12", [5, 5, 40], 30, padding="VALID", trainable=trainable)
            upconv13 = conv.TransposedConv2DLayer(upconv12.getOutputTensor(), "upconv13", [5, 5, 30], 20, padding="VALID", trainable=trainable)
            upconv14 = conv.TransposedConv2DLayer(upconv13.getOutputTensor(), "upconv14", [7, 7, 20], 10, padding="VALID", trainable=trainable)
            self.layers["upconv11"] = upconv11
            self.layers["upconv12"] = upconv12
            self.layers["upconv13"] = upconv13
            self.layers["upconv14"] = upconv14

            prepad = tf.pad(upconv14.getOutputTensor(), [[0, 0], [3, 3], [3, 3], [0, 0]])
            predict = conv.Conv2DLayer(prepad, "predict", [7, 7, 10], 2, padding="VALID", trainable=trainable)
            self.layers["predict"] = predict
            self.output=predict.getOutputTensor()
            self.global_step = tf.get_variable("global_step", shape=[], dtype=tf.int64, initializer=tf.zeros_initializer())



class FlowNetBrickModel5(FlowNetModelBase):
    name_type ="DILATFLOWNET"
    def __init__(self):
        self.layers = {}
        return
    
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        self.model_scope_build=model_scope
        with tf.variable_scope(model_scope):
            #dilation = 1, o=i-16
            conv11 = conv.Conv2DLayer(input, "conv11", [5, 5, input_channels], 60, padding="VALID", activation="RELU", trainable=trainable)
            conv12 = conv.Conv2DLayer(conv11.getOutputTensor(), "conv12", [5, 5, 60], 80, padding="VALID", activation="RELU", trainable=trainable)
            conv13 = conv.Conv2DLayer(conv12.getOutputTensor(), "conv13", [5, 5, 80], 120, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv11"] = conv11
            self.layers["conv12"] = conv12
            self.layers["conv13"] = conv13

            #dilation = 2, o=i-24
            conv21 = conv.Conv2DLayer(input, "conv21", [5, 5, input_channels], 60, dilation=2, padding="VALID", activation="RELU", trainable=trainable)
            conv22 = conv.Conv2DLayer(conv21.getOutputTensor(), "conv22", [5, 5, 60], 80, dilation=2, padding="VALID", activation="RELU", trainable=trainable)
            conv23 = conv.Conv2DLayer(conv22.getOutputTensor(), "conv23", [5, 5, 80], 120, dilation=2, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv21"] = conv21
            self.layers["conv22"] = conv22
            self.layers["conv23"] = conv23

            #dilation = 4, o=i-48
            conv31 = conv.Conv2DLayer(input, "conv31", [5, 5, input_channels], 60, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            conv32 = conv.Conv2DLayer(conv31.getOutputTensor(), "conv32", [5, 5, 60],80, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            conv33 = conv.Conv2DLayer(conv32.getOutputTensor(), "conv33", [5, 5, 80], 120, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv31"] = conv31
            self.layers["conv32"] = conv32
            self.layers["conv33"] = conv33

            #dilation = 6, o=i-72
            conv41 = conv.Conv2DLayer(input, "conv41", [5, 5, input_channels], 60, dilation=6, padding="VALID", activation="RELU", trainable=trainable)
            conv42 = conv.Conv2DLayer(conv41.getOutputTensor(), "conv42", [5, 5, 60],80, dilation=6, padding="VALID", activation="RELU", trainable=trainable)
            conv43 = conv.Conv2DLayer(conv42.getOutputTensor(), "conv43", [5, 5, 80], 120, dilation=6, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv41"] = conv41
            self.layers["conv42"] = conv42
            self.layers["conv43"] = conv43

            pad_conv43 = tf.pad(conv43.getOutputTensor(), [[0, 0], [9, 9], [9, 9], [0, 0]])
            upconv43 = conv.TransposedConv2DLayer(pad_conv43, "upconv43", [7, 7, 120], 80, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["upconv43"] = upconv43

            concat1 = tf.concat([conv33.getOutputTensor(), upconv43.getOutputTensor()], axis=3)
            pad_conv33 = tf.pad(concat1, [[0, 0], [9, 9], [9, 9], [0, 0]])
            upconv33 = conv.TransposedConv2DLayer(pad_conv33, "upconv33", [7, 7, 120+80], 60, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["upconv33"] = upconv33

            concat2 = tf.concat([conv23.getOutputTensor(), upconv33.getOutputTensor()], axis=3)
            pad_conv23 = tf.pad(concat2, [[0, 0], [3, 3], [3, 3], [0, 0]])
            upconv23 = conv.TransposedConv2DLayer(pad_conv23, "upconv23", [7, 7, 120+60], 40, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["upconv23"] = upconv23

            concat3 = tf.concat([conv13.getOutputTensor(), upconv23.getOutputTensor()], axis=3)
            
            upconvf1 = conv.TransposedConv2DLayer(concat3, "upconvf1", [5, 5, 120+40], 30, padding="VALID", activation="RELU", trainable=trainable)
            upconvf2 = conv.TransposedConv2DLayer(upconvf1.getOutputTensor(), "upconvf2", [5, 5, 30], 20, activation="RELU", padding="VALID", trainable=trainable)
            upconvf3 = conv.TransposedConv2DLayer(upconvf2.getOutputTensor(), "upconvf3", [5, 5, 20], 10, activation="RELU", padding="VALID", trainable=trainable)
            self.layers["upconvf1"] = upconvf1
            self.layers["upconvf2"] = upconvf2
            self.layers["upconvf3"] = upconvf3

            prepad = tf.pad(upconvf3.getOutputTensor(), [[0, 0], [3, 3], [3, 3], [0, 0]])
            predict = conv.Conv2DLayer(prepad, "predict", [7, 7, 10], 2, padding="VALID", trainable=trainable)
            self.layers["predict"] = predict

            self.output=predict.getOutputTensor()
            self.global_step = tf.get_variable("global_step", shape=[], dtype=tf.int64, initializer=tf.zeros_initializer(), trainable=False)


class FlowNetBrickModel7(FlowNetModelBase):
    name_type ="DIFLOWNET_LR"
    def __init__(self):
        self.layers = {}
        return
    
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        self.model_scope_build=model_scope
        with tf.variable_scope(model_scope):
            #dilation = 1, o=i-16
            conv11 = conv.Conv2DLayer(input, "conv11", [5, 5, input_channels], 60, padding="VALID", activation="LRELU", trainable=trainable)
            conv12 = conv.Conv2DLayer(conv11.getOutputTensor(), "conv12", [5, 5, 60], 80, padding="VALID", activation="LRELU", trainable=trainable)
            conv13 = conv.Conv2DLayer(conv12.getOutputTensor(), "conv13", [5, 5, 80], 120, padding="VALID", activation="LRELU", trainable=trainable)
            self.layers["conv11"] = conv11
            self.layers["conv12"] = conv12
            self.layers["conv13"] = conv13

            #dilation = 2, o=i-24
            conv21 = conv.Conv2DLayer(input, "conv21", [5, 5, input_channels], 60, dilation=2, padding="VALID", activation="LRELU", trainable=trainable)
            conv22 = conv.Conv2DLayer(conv21.getOutputTensor(), "conv22", [5, 5, 60], 80, dilation=2, padding="VALID", activation="LRELU", trainable=trainable)
            conv23 = conv.Conv2DLayer(conv22.getOutputTensor(), "conv23", [5, 5, 80], 120, dilation=2, padding="VALID", activation="LRELU", trainable=trainable)
            self.layers["conv21"] = conv21
            self.layers["conv22"] = conv22
            self.layers["conv23"] = conv23

            #dilation = 4, o=i-48
            conv31 = conv.Conv2DLayer(input, "conv31", [5, 5, input_channels], 60, dilation=4, padding="VALID", activation="LRELU", trainable=trainable)
            conv32 = conv.Conv2DLayer(conv31.getOutputTensor(), "conv32", [5, 5, 60],80, dilation=4, padding="VALID", activation="LRELU", trainable=trainable)
            conv33 = conv.Conv2DLayer(conv32.getOutputTensor(), "conv33", [5, 5, 80], 120, dilation=4, padding="VALID", activation="LRELU", trainable=trainable)
            self.layers["conv31"] = conv31
            self.layers["conv32"] = conv32
            self.layers["conv33"] = conv33

            #dilation = 6, o=i-72
            conv41 = conv.Conv2DLayer(input, "conv41", [5, 5, input_channels], 60, dilation=6, padding="VALID", activation="LRELU", trainable=trainable)
            conv42 = conv.Conv2DLayer(conv41.getOutputTensor(), "conv42", [5, 5, 60],80, dilation=6, padding="VALID", activation="LRELU", trainable=trainable)
            conv43 = conv.Conv2DLayer(conv42.getOutputTensor(), "conv43", [5, 5, 80], 120, dilation=6, padding="VALID", activation="LRELU", trainable=trainable)
            self.layers["conv41"] = conv41
            self.layers["conv42"] = conv42
            self.layers["conv43"] = conv43

            pad_conv43 = tf.pad(conv43.getOutputTensor(), [[0, 0], [9, 9], [9, 9], [0, 0]])
            upconv43 = conv.TransposedConv2DLayer(pad_conv43, "upconv43", [7, 7, 120], 80, padding="VALID", activation="LRELU", trainable=trainable)
            self.layers["upconv43"] = upconv43

            concat1 = tf.concat([conv33.getOutputTensor(), upconv43.getOutputTensor()], axis=3)
            pad_conv33 = tf.pad(concat1, [[0, 0], [9, 9], [9, 9], [0, 0]])
            upconv33 = conv.TransposedConv2DLayer(pad_conv33, "upconv33", [7, 7, 120+80], 60, padding="VALID", activation="LRELU", trainable=trainable)
            self.layers["upconv33"] = upconv33

            concat2 = tf.concat([conv23.getOutputTensor(), upconv33.getOutputTensor()], axis=3)
            pad_conv23 = tf.pad(concat2, [[0, 0], [3, 3], [3, 3], [0, 0]])
            upconv23 = conv.TransposedConv2DLayer(pad_conv23, "upconv23", [7, 7, 120+60], 40, padding="VALID", activation="LRELU", trainable=trainable)
            self.layers["upconv23"] = upconv23

            concat3 = tf.concat([conv13.getOutputTensor(), upconv23.getOutputTensor()], axis=3)
            
            upconvf1 = conv.TransposedConv2DLayer(concat3, "upconvf1", [5, 5, 120+40], 30, padding="VALID", activation="LRELU", trainable=trainable)
            upconvf2 = conv.TransposedConv2DLayer(upconvf1.getOutputTensor(), "upconvf2", [5, 5, 30], 20, activation="LRELU", padding="VALID", trainable=trainable)
            upconvf3 = conv.TransposedConv2DLayer(upconvf2.getOutputTensor(), "upconvf3", [5, 5, 20], 10, activation="LRELU", padding="VALID", trainable=trainable)
            self.layers["upconvf1"] = upconvf1
            self.layers["upconvf2"] = upconvf2
            self.layers["upconvf3"] = upconvf3

            prepad = tf.pad(upconvf3.getOutputTensor(), [[0, 0], [3, 3], [3, 3], [0, 0]])
            predict = conv.Conv2DLayer(prepad, "predict", [7, 7, 10], 2, padding="VALID", trainable=trainable)
            self.layers["predict"] = predict

            self.output=predict.getOutputTensor()
            self.global_step = tf.get_variable("global_step", shape=[], dtype=tf.int64, initializer=tf.zeros_initializer(), trainable=False)


class FlowNetBrickModel6(FlowNetModelBase):
    name_type ="DILATFLOWNET2"
    def __init__(self):
        self.layers = {}
        return
    
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        self.model_scope_build=model_scope
        with tf.variable_scope(model_scope):
            #dilation = 1, o=i-16
            conv11 = conv.Conv2DLayer(input, "conv11", [5, 5, input_channels], 60, padding="VALID", activation="RELU", trainable=trainable)
            conv12 = conv.Conv2DLayer(conv11.getOutputTensor(), "conv12", [5, 5, 60], 80, padding="VALID", activation="RELU", trainable=trainable)
            conv13 = conv.Conv2DLayer(conv12.getOutputTensor(), "conv13", [5, 5, 80], 120, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv11"] = conv11
            self.layers["conv12"] = conv12
            self.layers["conv13"] = conv13

            #dilation = 2, o=i-24
            conv21 = conv.Conv2DLayer(input, "conv21", [5, 5, input_channels], 60, dilation=2, padding="VALID", activation="RELU", trainable=trainable)
            conv22 = conv.Conv2DLayer(conv21.getOutputTensor(), "conv22", [5, 5, 60], 80, dilation=2, padding="VALID", activation="RELU", trainable=trainable)
            conv23 = conv.Conv2DLayer(conv22.getOutputTensor(), "conv23", [5, 5, 80], 120, dilation=2, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv21"] = conv21
            self.layers["conv22"] = conv22
            self.layers["conv23"] = conv23

            #dilation = 4, o=i-48
            conv31 = conv.Conv2DLayer(input, "conv31", [5, 5, input_channels], 60, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            conv32 = conv.Conv2DLayer(conv31.getOutputTensor(), "conv32", [5, 5, 60],80, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            conv33 = conv.Conv2DLayer(conv32.getOutputTensor(), "conv33", [5, 5, 80], 120, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv31"] = conv31
            self.layers["conv32"] = conv32
            self.layers["conv33"] = conv33

            #dilation = 6, o=i-72
            conv41 = conv.Conv2DLayer(input, "conv41", [5, 5, input_channels], 60, dilation=6, padding="VALID", activation="RELU", trainable=trainable)
            conv42 = conv.Conv2DLayer(conv41.getOutputTensor(), "conv42", [5, 5, 60],80, dilation=6, padding="VALID", activation="RELU", trainable=trainable)
            conv43 = conv.Conv2DLayer(conv42.getOutputTensor(), "conv43", [5, 5, 80], 120, dilation=6, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv41"] = conv41
            self.layers["conv42"] = conv42
            self.layers["conv43"] = conv43

            pad_conv43 = tf.pad(conv43.getOutputTensor(), [[0, 0], [9, 9], [9, 9], [0, 0]])
            upconv43 = conv.TransposedConv2DLayer(pad_conv43, "upconv43", [7, 7, 120], 80, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["upconv43"] = upconv43

            concat1 = tf.concat([conv33.getOutputTensor(), upconv43.getOutputTensor()], axis=3)
            pad_conv33 = tf.pad(concat1, [[0, 0], [9, 9], [9, 9], [0, 0]])
            upconv33 = conv.TransposedConv2DLayer(pad_conv33, "upconv33", [7, 7, 120+80], 60, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["upconv33"] = upconv33

            concat2 = tf.concat([conv23.getOutputTensor(), upconv33.getOutputTensor()], axis=3)
            pad_conv23 = tf.pad(concat2, [[0, 0], [3, 3], [3, 3], [0, 0]])
            upconv23 = conv.TransposedConv2DLayer(pad_conv23, "upconv23", [7, 7, 120+60], 40, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["upconv23"] = upconv23

            concat3 = tf.concat([conv13.getOutputTensor(), upconv23.getOutputTensor()], axis=3)
            
            upconvf1 = conv.TransposedConv2DLayer(concat3, "upconvf1", [5, 5, 120+40], 30, padding="VALID", activation="RELU", trainable=trainable)
            upconvf2 = conv.TransposedConv2DLayer(upconvf1.getOutputTensor(), "upconvf2", [5, 5, 30], 20, activation="RELU", padding="VALID", trainable=trainable)
            upconvf3 = conv.TransposedConv2DLayer(upconvf2.getOutputTensor(), "upconvf3", [5, 5, 20], 10, activation="RELU", padding="VALID", trainable=trainable)
            self.layers["upconvf1"] = upconvf1
            self.layers["upconvf2"] = upconvf2
            self.layers["upconvf3"] = upconvf3

            prepad = tf.pad(upconvf3.getOutputTensor(), [[0, 0], [3, 3], [3, 3], [0, 0]])
            predict = conv.Conv2DLayer(prepad, "predict", [7, 7, 10], 2, padding="VALID", trainable=trainable)
            self.layers["predict"] = predict

            self.output=predict.getOutputTensor()
            self.global_step = tf.get_variable("global_step", shape=[], dtype=tf.int64, initializer=tf.zeros_initializer(), trainable=False)



class DilatFlowNet3(FlowNetModelBase):
    name_type ="DILATFLOWNET3"
    def __init__(self):
        self.layers = {}
        return
    
    def build_model(self, input, input_channels, model_scope, trainable=None, args=None, keep_prob=None):
        self.model_scope_build=model_scope
        with tf.variable_scope(model_scope):
            #dilation = 1, o=i-20
            conv11 = conv.Conv2DLayer(input, "conv11", [5, 5, input_channels], 60, padding="VALID", activation="RELU", trainable=trainable)
            conv12 = conv.Conv2DLayer(conv11.getOutputTensor(), "conv12", [5, 5, 60], 80, padding="VALID", activation="RELU", trainable=trainable)
            conv13 = conv.Conv2DLayer(conv12.getOutputTensor(), "conv13", [5, 5, 80], 120, padding="VALID", activation="RELU", trainable=trainable)
            conv14 = conv.Conv2DLayer(conv13.getOutputTensor(), "conv14", [5, 5, 120], 160, padding="VALID", activation="RELU", trainable=trainable)
            conv15 = conv.Conv2DLayer(conv14.getOutputTensor(), "conv15", [5, 5, 160], 180, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv11"] = conv11
            self.layers["conv12"] = conv12
            self.layers["conv13"] = conv13
            self.layers["conv14"] = conv14
            self.layers["conv15"] = conv15


            #dilation = 4, o=i-80
            conv31 = conv.Conv2DLayer(input, "conv31", [5, 5, input_channels], 60, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            conv32 = conv.Conv2DLayer(conv31.getOutputTensor(), "conv32", [5, 5, 60],80, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            conv33 = conv.Conv2DLayer(conv32.getOutputTensor(), "conv33", [5, 5, 80], 120, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            conv34 = conv.Conv2DLayer(conv33.getOutputTensor(), "conv34", [5, 5, 120], 160, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            conv35 = conv.Conv2DLayer(conv34.getOutputTensor(), "conv35", [5, 5, 160], 180, dilation=4, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["conv31"] = conv31
            self.layers["conv32"] = conv32
            self.layers["conv33"] = conv33
            self.layers["conv34"] = conv34 
            self.layers["conv35"] = conv35


            pad_5 = tf.pad(conv35.getOutputTensor(), [[0, 0], [27, 27], [27, 27], [0, 0]])
            upd_35to15 = conv.TransposedConv2DLayer(pad_5, "upd_35to15", [7, 7, 180], 80, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["upd_35to15"]=upd_35to15
            concat_5 = tf.concat([conv15.getOutputTensor(), upd_35to15.getOutputTensor()], axis=3)
            
            up_5to4 = conv.TransposedConv2DLayer(concat_5, "upd_5to4", [5, 5, 180+80], 80, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["up_5to4"]=up_5to4

            pad_4 = tf.pad(conv34.getOutputTensor(), [[0, 0], [21, 21], [21, 21], [0, 0]])
            upd_34to14 = conv.TransposedConv2DLayer(pad_4, "upd_34to14", [7, 7, 160], 60, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["upd_34to14"]=upd_34to14
            concat_4 = tf.concat([conv14.getOutputTensor(), upd_34to14.getOutputTensor()], axis=3)

            concat4 = tf.concat([up_5to4.getOutputTensor(), concat_4], axis=3)
            up_4to3 = conv.TransposedConv2DLayer(concat4, "upd_4to3", [5, 5, 160+80+60], 60, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["up_4to3"]=up_4to3

            pad_3 = tf.pad(conv33.getOutputTensor(), [[0, 0], [15, 15], [15, 15], [0, 0]])
            upd_33to13 = conv.TransposedConv2DLayer(pad_3, "upd_33to13", [7, 7, 120], 40, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["upd_33to13"]=upd_33to13
            concat_3 = tf.concat([conv13.getOutputTensor(), upd_33to13.getOutputTensor()], axis=3)
            
            concat3 = tf.concat([up_4to3.getOutputTensor(), concat_3], axis=3)
            up_3to2 = conv.TransposedConv2DLayer(concat3, "up_3to2", [5, 5, 120+60+40], 80, padding="VALID", activation="RELU", trainable=trainable)
            self.layers["up_3to2"]=up_3to2

            up_2to1 = conv.TransposedConv2DLayer(up_3to2.getOutputTensor(), "up_2to1", [5, 5, 80], 40, padding="VALID", trainable=trainable)
            self.layers["up_2to1"]=up_2to1
            predict = conv.TransposedConv2DLayer(up_2to1.getOutputTensor(), "predict", [5, 5, 40], 2, padding="VALID", trainable=trainable) 
            self.layers["predict"]=predict

            self.output=predict.getOutputTensor()
            self.global_step = tf.get_variable("global_step", shape=[], dtype=tf.int64, initializer=tf.zeros_initializer(), trainable=False)

    