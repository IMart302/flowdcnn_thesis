# coding=utf-8

import serial_flownet as snet
import flow_models_base as fmods 
import dataset_utils as fds
import os

#windows
#dataset_path = "C:\LinuxNexum\Documents\FlyingChairs_release\data"
#linux
dataset_path = "/home/ivan/Datasets/FlyingChairs_release/data"

"""
name_model = "STM5"
#se debe asegurar que tenga igual modelos (menos uno si es nuevo) que stack
#como no hay ningun modelo entrenado se deja vacio
#models_ckpt_paths = []
models_ckpt_paths = ["N1"]
train_schedule = {"epochs":500, "valid_step":1, "learning_rate": 0.0001, "batches_lim": 30}
save_schedule = {"save_epoch":1, "save_batch_step":10}
chairs_ds = fds.ChairsDatasetManager(dataset_path, 21000, 22000, 22872, 1, shuffle=False)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""


"""
name_model = "STM6"
models_ckpt_paths = []
train_schedule = {"epochs":500, "valid_step":1, "learning_rate": 0.0001, "batches_lim": 30}
save_schedule = {"save_epoch":1, "save_batch_step":20}
chairs_ds = fds.ChairsDatasetManager(dataset_path, 21000, 22000, 22872, 1, shuffle=False)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel3_0()]
"""

"""
name_model = "STM6_0"
models_ckpt_paths = []
train_schedule = {"epochs":100, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":20}
chairs_ds = fds.ChairsDatasetManager(dataset_path, 21000, 22000, 22872, 1, shuffle=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel3_0()]
"""

"""
name_model = "STM6_0_debug"
models_ckpt_paths = []
train_schedule = {"epochs":40, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":10}
chairs_ds = fds.ChairsDatasetManager(dataset_path, 20, 22000, 22872, 1, shuffle=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel3_0()]
"""

"""
name_model = "STM5_debug"
models_ckpt_paths = []
train_schedule = {"epochs":30, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":10}
chairs_ds = fds.ChairsDatasetManager(dataset_path, 20, 22000, 22872, 1, shuffle=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

"""
name_model = "STM5_debugK1"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":10}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 30]], [[31, 40]], None, 1, shuffle=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

"""
name_model = "STM5_debugK2"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":10}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 20], [31, 40]], [[21, 30]], None, 1, shuffle=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

"""
name_model = "STM5_debugK3"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":10}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 10], [21, 40]], [[11, 20]], None, 1, shuffle=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

"""
name_model = "STM5_debugK4"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":10}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[11, 40]], [[1, 10]], None, 1, shuffle=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""
"""
name_model = "STM5_debugK1_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 150]], [[151, 200]], None, 1, shuffle=False)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

"""
name_model = "STM5_debugK2_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 100], [151, 200]], [[101, 150]], None, 1, shuffle=False)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

"""
name_model = "STM5_debugK3_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 50], [101, 200]], [[51, 100]], None, 1, shuffle=False)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

"""
name_model = "STM5_debugK4_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.0001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[51, 200]], [[1, 50]], None, 1, shuffle=False)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

"""
name_model = "SNET1_debugK1X10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 180]], [[181, 200]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.NaiveFlowNetModel2()]
"""

"""
name_model = "SNET2_debugK1x10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 180]], [[181, 200]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET2_debugK2x10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 160], [181, 200]], [[161, 180]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""
"""
name_model = "SNET2_debugK3x10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 140], [161, 200]], [[141, 160]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""
"""
name_model = "SNET2_debugK4x10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 120], [141, 200]], [[121, 140]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET2_debugK5x10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 100], [121, 200]], [[101, 120]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET2_debugK6x10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 80], [101, 200]], [[81, 100]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET2_debugK7x10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 60], [81, 200]], [[61, 80]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET2_debugK8x10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 40], [61, 200]], [[41, 60]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""
"""
name_model = "SNET2_debugK9x10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 20], [41, 200]], [[21, 40]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET2_debugK10x10_200"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.00001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[21, 200]], [[1, 20]], 1, shuffle=False, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET_MSE_debugK1x4_1000"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.000001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 750]], [[751, 1000]], 1, shuffle=True, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET_MSE_debugK2x4_1000"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.000001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 500], [751, 1000]], [[501, 750]], 1, shuffle=True, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET_MSE_debugK3x4_1000"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.000001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 250], [501, 1000]], [[251, 500]], 1, shuffle=True, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET_MSE_debugK4x4_1000"
models_ckpt_paths = []
train_schedule = {"epochs":50, "valid_step":1, "learning_rate": 0.000001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[251, 1000]], [[1, 250]], 1, shuffle=True, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET_MSE_LONG_debug_2000"
models_ckpt_paths = []
train_schedule = {"epochs":200, "valid_step":1, "learning_rate": 0.000001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 2000]], [[2001, 2200]], 1, shuffle=True, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

"""
name_model = "SNET_CTRAIN_debug_2000"
models_ckpt_paths = []
train_schedule = {"epochs":200, "valid_step":1, "learning_rate": 0.000001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 2000]], [[2001, 2200]], 1, shuffle=True, fragmentate=2, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]
"""

name_model = "SNET_SANITYCHECK_TRAIN_debug"
models_ckpt_paths = []
train_schedule = {"epochs":2, "valid_step":1, "optimizer": "ADAM", "learning_rate": 0.000001}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, [[1, 2]], [[3, 4]], 1, shuffle=True, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel()]


shape = chairs_ds.getFinalShape()
mflownet.model_build(shape[0], shape[1], mods, True)
mflownet.init_session()
mflownet.model_session_chkpt_restore(models_ckpt_paths)
mflownet.train(chairs_ds, train_schedule, save_schedule)
mflownet.close_session()
