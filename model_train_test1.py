# coding=utf-8

import stack_flownet as snet
import os 
import flow_models_base as fmods 
import dataset_utils as fds

#windows
#dataset_path = "C:\LinuxNexum\Documents\FlyingChairs_release\data"
#linux
dataset_path = "/home/ivan/Datasets/FlyingChairs_release/data"


"""
name_model = "STM5"
#se debe asegurar que tenga igual modelos (menos uno si es nuevo) que stack
#como no hay ningun modelo entrenado se deja vacio
#models_ckpt_paths = []
models_ckpt_paths = ["N1"]
train_schedule = {"epochs":2, "valid_step":1, "learning_rate": 0.0001, "batches_lim": 30}
save_schedule = {"save_epoch":1, "save_batch_step":10}
chairs_ds = fds.ChairsDatasetManager(dataset_path, 21000, 22000, 22872, 1, shuffle=False)
mflownet = snet.FlowNetStackManager(name_model)
fmods = [fmods.NaiveFlowNetModel2(), fmods.NaiveFlowNetModel2()]
"""

name_model = "STM6"
models_ckpt_paths = ["N1"]
train_schedule = {"epochs":2, "valid_step":1, "learning_rate": 0.0001, "batches_lim": 30}
save_schedule = {"save_epoch":1, "save_batch_step":10}
chairs_ds = fds.ChairsDatasetManager(dataset_path, 21000, 22000, 22872, 1, shuffle=False)
mflownet = snet.FlowNetStackManager(name_model)
fmods = [fmods.NaiveFlowNetModel2(), fmods.NaiveFlowNetModel2()]



mflownet.model_build(384, 512, fmods, True)
mflownet.init_session()
mflownet.model_session_chkpt_restore(models_ckpt_paths)
mflownet.train(chairs_ds, train_schedule, save_schedule)
mflownet.close_session()