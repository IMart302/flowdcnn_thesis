# coding=utf-8

import numpy as np 
import matplotlib.pyplot as plt
import csv
import os 


#mname = "STM5_cost_log2018-11-18-17-12-06.txt"
#mname = "STM5_cost_log2018-11-19-16-09-28.txt"
#mname = "STM5_cost_log2018-11-21-21-21-03.txt"
#mname = "STM6_0_cost_log_2019-01-18-01-17-40.txt"
#mname = "STM6_0_debug_cost_log_2019-02-04-23-30-19.txt"
#mname = "STM5_debug_cost_log_2019-02-05-20-47-58.txt"
mname = "STM5_debug_cost_log_2019-02-06-00-26-00.txt"
#mname = "STM6_0_debug_cost_log_2019-02-06-19-15-10.txt"
srows = 5

x = []
y = []

cwd = os.getcwd()
filename=cwd+"/"+mname

xi = 0
rcount = 0
with open(filename,'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    for row in plots:
        if(rcount < srows):
            rcount = rcount + 1
        else:
            x.append(float(xi))
            y.append(float(row[0]))
            xi = xi + 1


plt.plot(x,y)
plt.xlabel('Batch step')
plt.ylabel('Costo')
plt.title(mname)
plt.legend()
plt.show()