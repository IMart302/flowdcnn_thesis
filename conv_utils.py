# coding=utf-8
import tensorflow as tf 
import numpy as np 


def lrelu(x, leak=0.2, name="lrelu"):
     with tf.variable_scope(name):
         f1 = 0.5 * (1 + leak)
         f2 = 0.5 * (1 - leak)
         return f1 * x + f2 * abs(x)

class Conv2DLayerBase(object):
    def __init__(self):
        self.weights = None
        self.bias = None
        self.conv_out = None
    
    def getOutputTensor(self):
        return self.conv_out

    def getWeights(self):
        return self.weights
    
    def getBiases(self):
        return self.bias

    def forgetOutput(self):
        self.conv_out = None


class Conv2DLayer(Conv2DLayerBase):

    def __init__(self, input, scope, filter_shape, filters, strides=1, activation = None, dilation = None, padding="SAME", trainable=None, regularizer="NONE", scale=0.01):

        with tf.variable_scope(scope):
            #define el filtro de convolucion 
            reg = None
            if(regularizer == "L2"):
                reg = tf.contrib.layers.l2_regularizer(scale=scale)
            
            if(regularizer == "L1"):
                reg = tf.contrib.layers.l1_regularizer(scale=scale)

            self.weights = tf.get_variable("weights", 
                                           shape=[filter_shape[0], filter_shape[1], filter_shape[2], filters], 
                                           initializer=tf.contrib.layers.xavier_initializer(), 
                                           trainable=trainable,
                                           regularizer=reg)

            #define el bias de la capa
            self.bias = tf.get_variable("bias", 
                                        shape=[filters], 
                                        initializer=tf.contrib.layers.xavier_initializer(),
                                        trainable=trainable)

            #si existe dilatacion usa atrous_convolution, sino usa convolución normal
            if(dilation == None):
                self.conv_out = tf.nn.conv2d(input, self.weights, strides=[1, strides, strides, 1], padding=padding)
            else:
                self.conv_out = tf.nn.atrous_conv2d(input, self.weights, rate=dilation, padding=padding)


            self.conv_out = tf.nn.bias_add(self.conv_out, self.bias)
        
            if(activation != None):
                if(activation == "ELU"):
                    self.conv_out = tf.nn.elu(self.conv_out)
                elif(activation == "RELU"):
                    self.conv_out = tf.nn.relu(self.conv_out)
                elif(activation == "LRELU"):
                    self.conv_out = lrelu(self.conv_out)


#calcula el shape de salida
class TransposedConv2DLayer(Conv2DLayerBase):
    def __init__(self, input, scope, filter_shape, filters, strides=1, activation=None, padding="SAME", trainable=None, regularizer="NONE", scale=0.01):
        with tf.variable_scope(scope):
            #out_shape = input.get_shape().as_list()
            #define la variable para almacenar los pesos de los filtros
            #para convolucion transpues el formato de los pesos es (row, col, #filtros o out channels, in channels)
            reg = None
            if(regularizer == "L2"):
                reg = tf.contrib.layers.l2_regularizer(scale=scale)
            
            if(regularizer == "L1"):
                reg = tf.contrib.layers.l1_regularizer(scale=scale)

            self.weights = tf.get_variable("weights", 
                                           shape=[filter_shape[0], filter_shape[1], filters, filter_shape[2]],
                                           initializer=tf.contrib.layers.xavier_initializer(),
                                           trainable=trainable,
                                           regularizer=reg)
            
            #define la viariable para almacenar los biases
            self.bias = tf.get_variable("bias", shape=[filters], initializer=tf.contrib.layers.xavier_initializer(), trainable=trainable)

            ishape = tf.shape(input)
            if(padding != "SAME"):
                owidth = strides*(ishape[1] - 1) + filter_shape[0]
                oheight = strides*(ishape[2] - 1) + filter_shape[1]
            else:
                owidth = ishape[1]
                oheight = ishape[2]

            tconv = tf.nn.conv2d_transpose(input, self.weights, strides=[1, strides, strides, 1], padding=padding, name="conv2d", output_shape=[ishape[0], owidth, oheight, filters])

            self.conv_out = tf.nn.bias_add(tconv, self.bias)
        
            if(activation != None):
                if(activation == "ELU"):
                    self.conv_out = tf.nn.elu(self.conv_out)
                elif(activation == "RELU"):
                    self.conv_out = tf.nn.relu(self.conv_out)
                elif(activation == "LRELU"):
                    self.conv_out = lrelu(self.conv_out)
