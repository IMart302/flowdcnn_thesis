# coding=utf-8

import stack_flownet as snet
import handy_flow_routines as futils
import flow_models_base as fmods
import os
import numpy as np

#dataset_path = "C:\LinuxNexum\Documents\FlyingChairs_release\data"
dataset_path = "/home/ivan/Datasets/FlyingChairs_release/data"
#frame1_path = "/home/ivan/Datasets/FlyingChairs_examples/0000000-img0.ppm"
#frame2_path = "/home/ivan/Datasets/FlyingChairs_examples/0000000-img1.ppm"
frame1_path = "/home/ivan/Datasets/FlyingChairs_release/data/00004_img1.ppm"
frame2_path = "/home/ivan/Datasets/FlyingChairs_release/data/00004_img2.ppm"

frame1 = futils.read(frame1_path)
frame2 = futils.read(frame2_path)

model_name="STM6"

"""
mflownet = snet.FlowNetStackManager("STM5")
models_ckpt_paths = ["N1"]
modlist = [fmods.NaiveFlowNetModel2()]
mflownet.model_build(frame1.shape[0], frame2.shape[1], modlist, False)
"""


mflownet = snet.FlowNetStackManager(model_name)
models_ckpt_paths = ["N1"]
modlist = [fmods.NaiveFlowNetModel3_0()]
mflownet.model_build(frame1.shape[0], frame2.shape[1], modlist, False)


mflownet.init_session()
mflownet.model_session_chkpt_restore(models_ckpt_paths)
flowes = np.array(mflownet.inference(frame1, frame2))
print(flowes.shape)
flowes = np.reshape(flowes, [flowes.shape[2], flowes.shape[3], flowes.shape[4]])
mflownet.close_session()

futils.writeFlow("test_flow.flo", flowes)