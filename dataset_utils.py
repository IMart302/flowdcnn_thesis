import numpy as np
import random
import handy_flow_routines as flowutils
import os
import csv

#dataset_flow_characteristics
flow_ds_utils = {
    "img_rows": 384,
    "img_cols": 512,
    "img_channels": 3,
    "flo_channels": 2,
    "img_ext": ".ppm",
    "flo_ext": ".flo",
    "num_exam": 22872
}

class FlowDatasetManagerBase(object):

    def __init__(self, data_dir, train_ext_split, valid_ext_split, batchsize, shuffle=True, fragmentate=None, resize=None, load_in_ram=False, valid_batch_size=1, normalize=False):
        self.img_subfix1 = "_img1.png"
        self.img_subfix2 = "_img2.png"
        self.flo_subfix = "_flow.flo"
        self.initSubIndexes()
        self.data_dir = data_dir
        self.train_split = train_ext_split
        self.valid_split = valid_ext_split
        self.batchsize = batchsize
        self.valid_batchsize = valid_batch_size
        self.shuffle = shuffle
        self.unsorted_ext_indx = []
        #referencia auxiliar para cambiar de ordenado a desordenado
        self.unsindx_aux = None
        self.valid_ext_indx = []
        self.resize = resize
        self.fragmentate = fragmentate
        self.loaded_in_ram = load_in_ram
        self.in_mem_train_example = []
        self.in_mem_train_target = []
        self.in_mem_test_example = []
        self.in_mem_test_target = []
        
        for ran in train_ext_split:
            self.unsorted_ext_indx = self.unsorted_ext_indx + list(range(ran[0], ran[1]+1))

        for ran in valid_ext_split:
            self.valid_ext_indx = self.valid_ext_indx + list(range(ran[0], ran[1] + 1))
        
        if(self.loaded_in_ram == True):
            for rindx in self.unsorted_ext_indx:
                img1, img2, flow = self.loadIndexedSample(rindx)
                if(normalize):
                    img1 = flowutils.normalize_image(img1)
                    img2 = flowutils.normalize_image(img2)

                imgs = np.concatenate((img1, img2), axis=2)
                #if(resize != None):
                if(fragmentate != None):
                    rows = img1.shape[0]
                    cols = img1.shape[1]
                    portionrows = int(rows/(fragmentate))
                    portioncols = int(cols/(fragmentate))
                    for i in range(0, fragmentate):
                        for j in range(0, fragmentate):
                            self.in_mem_train_example.append(imgs[i*portionrows:(i+1)*portionrows, j*portioncols:(j+1)*portioncols, :]) 
                            self.in_mem_train_target.append(flow[i*portionrows:(i+1)*portionrows, j*portioncols:(j+1)*portioncols, :])

                else:
                    self.in_mem_train_example.append(imgs)
                    self.in_mem_train_target.append(flow)

            for rindx in self.valid_ext_indx:
                img1, img2, flow = self.loadIndexedSample(rindx)

                if(normalize):
                    img1 = flowutils.normalize_image(img1)
                    img2 = flowutils.normalize_image(img2)
                    
                imgs = np.concatenate((img1, img2), axis=2)
                #if(resize != None):
                if(fragmentate != None):
                    rows = img1.shape[0]
                    cols = img1.shape[1]
                    portionrows = int(rows/(fragmentate))
                    portioncols = int(cols/(fragmentate))
                    for i in range(0, fragmentate):
                        for j in range(0, fragmentate):
                            self.in_mem_test_example.append(imgs[i*portionrows:(i+1)*portionrows, j*portioncols:(j+1)*portioncols, :]) 
                            self.in_mem_test_target.append(flow[i*portionrows:(i+1)*portionrows, j*portioncols:(j+1)*portioncols, :])

                else:
                    self.in_mem_test_example.append(imgs)
                    self.in_mem_test_target.append(flow)
            
            self.unsorted_ext_indx = list(range(0, len(self.in_mem_train_example)))
            self.valid_ext_indx = list(range(0, len(self.in_mem_test_example)))
        
        if(self.shuffle==True):
            random.shuffle(self.unsorted_ext_indx)
        
        self.last_indx_train = 0
        self.last_indx_val = 0
        self.last_indx_train2 = 0
        self.last_indx_val2 = 0
        self.sorted_ext_indx = self.unsorted_ext_indx.copy()
        self.sorted_ext_indx.sort()
        

    def initSubIndexes(self):
        raise NotImplementedError

    def getBatchSize(self):
        return self.batchsize    
    
    @staticmethod
    def trainTestSplitToListRange(file_path, limit=22000):
        train = []
        test = []
        train_test=1
        left = 1
        rowcount = 0
        with open(file_path,'r') as csvfile:
            plots = csv.reader(csvfile, delimiter=',')
            datarows = list(plots)
            for r in range(0, limit):
                row = datarows[r]
                val = int(row[0])
                if(val != train_test):
                    if(train_test == 1):
                        train_test=2
                        train.append([left, rowcount])
                    elif(train_test == 2):
                        train_test=1
                        test.append([left, rowcount])

                    left = rowcount + 1
                
                rowcount = rowcount + 1
            
            if(train_test == 1):
                train.append([left, rowcount])
            if(train_test == 2):
                test.append([left, rowcount])
        
        return train, test


    def loadIndexedSample(self, indx):
        
        if(indx < 10):
            fileindx = "0000"+str(indx)
        elif(indx < 100):
            fileindx = "000"+str(indx)
        elif(indx < 1000):
            fileindx = "00"+str(indx)
        elif(indx < 10000):
            fileindx = "0"+str(indx)
        else:
            fileindx = str(indx)

        img1dir = self.data_dir+"/"+fileindx+self.img_subfix1
        img2dir = self.data_dir+"/"+fileindx+self.img_subfix2
        flodir = self.data_dir+"/"+fileindx+self.flo_subfix
        img1 = flowutils.read(img1dir)
        img2 = flowutils.read(img2dir)
        flow = flowutils.read(flodir)
        
        return img1, img2, flow

    @staticmethod
    def loadSampleAt(data_dir, indx, subfixes=["_img1.ppm", "_img2.ppm","_flow.flo"]):
        
        if(indx < 10):
            fileindx = "0000"+str(indx)
        elif(indx < 100):
            fileindx = "000"+str(indx)
        elif(indx < 1000):
            fileindx = "00"+str(indx)
        elif(indx < 10000):
            fileindx = "0"+str(indx)
        else:
            fileindx = str(indx)

        img1dir = data_dir+"/"+fileindx+subfixes[0]
        img2dir = data_dir+"/"+fileindx+subfixes[1]
        flodir = data_dir+"/"+fileindx+subfixes[2]
        img1 = flowutils.read(img1dir)
        #print("RANK=========")
        #print(np.rank(img1))
        if(img1.ndim == 2):
            img1 = np.stack([img1, img1, img1], axis=-1)
        
        img2 = flowutils.read(img2dir)
        if(img2.ndim == 2):
            img2 = np.stack([img2, img2, img2], axis=-1)
            
        flow = flowutils.read(flodir)
        
        return img1, img2, flow
    

    #get example withput modify internal counters
    def getIndexedExample(self, index):
        if(index >= len(self.unsorted_ext_indx)):
            index = index % len(self.unsorted_ext_indx)
        
        if(self.loaded_in_ram==True):
            imgs = self.in_mem_train_example[index]
            flow = self.in_mem_train_target[index]
        else:
            rind = self.unsorted_ext_indx[index]
            img1, img2, flow = self.loadIndexedSample(rind)
            imgs = np.concatenate(img1, img2, axis=2)
        
        return imgs, flow

    #get example withput modify internal counters
    def getIndexedValidation(self, index):
        if(index >= len(self.valid_ext_indx)):
            index = index % len(self.valid_ext_indx)
        
        if(self.loaded_in_ram==True):
            imgs = self.in_mem_test_example[index]
            flow = self.in_mem_test_target[index]
        else:
            rind = self.valid_ext_indx[index]
            img1, img2, flow = self.loadIndexedSample(rind)
            imgs = np.concatenate(img1, img2, axis=2)
         
        return imgs, flow


    def nextFlowBatch(self, in_list=False):
        flowsls = []
        imgs_conc = []

        indx_mem = 0
        for i in range(self.last_indx_train, self.last_indx_train + self.batchsize):
            indx_mem = i
        
            if(i >= len(self.unsorted_ext_indx)):
                indx = self.unsorted_ext_indx[i%len(self.unsorted_ext_indx)]
            else:
                indx = self.unsorted_ext_indx[i]

            if(self.loaded_in_ram == True):
                imgs_conc.append(self.in_mem_train_example[indx])
                flowsls.append(self.in_mem_train_target[indx])
            else:
                img1, img2, flow = self.loadIndexedSample(indx)
                imgs_conc.append(np.concatenate(img1, img2, axis=2))
                flowsls.append(flow)
        
        #asegura que el indice memoria este en el rango
        if(indx_mem >= len(self.unsorted_ext_indx)):
            indx_mem = indx_mem % len(self.unsorted_ext_indx)
        
        self.last_indx_train = indx_mem + 1
        
 
        imgs = np.stack(imgs_conc, axis=0)
        flows = np.stack(flowsls, axis=0)
        return imgs, flows
    
    def nextValBatch(self):
        flowsls = []
        imgs_conc = []

        indx_mem = 0
        for i in range(self.last_indx_val, self.last_indx_val + self.valid_batchsize):
            indx_mem = i
            
            if(i >= len(self.valid_ext_indx)):
                indx = self.valid_ext_indx[i%len(self.valid_ext_indx)]
            else:
                indx = self.valid_ext_indx[i]

            if(self.loaded_in_ram == True):
                imgs_conc.append(self.in_mem_test_example[indx])
                flowsls.append(self.in_mem_test_target[indx])
            else:
                img1, img2, flow = self.loadIndexedSample(indx)
                imgs_conc.append(np.concatenate(img1, img2, axis=2))
                flowsls.append(flow)
        
        #asegura que el indice memoria este en el rango
        if(indx_mem >= len(self.valid_ext_indx)):
            indx_mem = indx_mem % len(self.valid_ext_indx)
        
        self.last_indx_val = indx_mem + 1
        
        imgs = np.stack(imgs_conc, axis=0)
        flows = np.stack(flowsls, axis=0)
        return imgs, flows

  
    def reset_indx(self):
        self.last_indx_train = 0
        self.last_indx_val = 0

    def shuffleTrain(self):
        if(self.shuffle == True):
            random.shuffle(self.unsorted_ext_indx)
    
    def getAvailableTrainBatches(self):
        return int(len(self.unsorted_ext_indx)/self.batchsize)
    
    def getAvailableValBatches(self):
        return int(len(self.valid_ext_indx)/self.batchsize)

    def getFinalShape(self):
        if(self.loaded_in_ram == True):
            shape = list(self.in_mem_train_example[0].shape)
        else:
            shape = [flow_ds_utils[384, 512, 3]]
        
        return shape

    def getTotalExamples(self):
        return len(self.unsorted_ext_indx)

    def getTotalValidation(self):
        return len(self.valid_ext_indx)
    
    def saveActualIndexes(self, sort_change = True):
        self.last_indx_train2 = self.last_indx_train
        self.last_indx_val2 = self.last_indx_val
        if(sort_change == True):
            self.unsindx_aux = self.unsorted_ext_indx
            self.unsorted_ext_indx = self.sorted_ext_indx
    
    def restorePreviousIndexes(self, sort_change = True):
        self.last_indx_train = self.last_indx_train2
        self.last_indx_val = self.last_indx_val2
        if(sort_change == True):
            self.unsorted_ext_indx = self.unsindx_aux



#clase para generar batches de ejemplos de flujo optico de flyingchairs
class ChairsDatasetManager(FlowDatasetManagerBase):
    def __init__(self, data_dir, train_ext_split, valid_ext_split, batchsize, shuffle=True, fragmentate=None, resize=None, load_in_ram=False, valid_batch_size=1, normalize=False):        
        super(ChairsDatasetManager, self).__init__(data_dir, train_ext_split, valid_ext_split, batchsize, shuffle=shuffle, fragmentate=fragmentate, resize=resize, load_in_ram=load_in_ram, valid_batch_size=valid_batch_size, normalize=normalize)

    def initSubIndexes(self):
        self.img_subfix1 = "_img1.ppm"
        self.img_subfix2 = "_img2.ppm"
        self.flo_subfix = "_flow.flo"

class SquareDatasetManager(ChairsDatasetManager) :

    def __init__(self, data_dir, train_ext_split, valid_ext_split, batchsize, shuffle=True, fragmentate=None, resize=None, load_in_ram=False, valid_batch_size=1, normalize=False):        
        super(SquareDatasetManager, self).__init__(data_dir, train_ext_split, valid_ext_split, batchsize, shuffle=shuffle, fragmentate=fragmentate, resize=resize, load_in_ram=load_in_ram, valid_batch_size=valid_batch_size, normalize=normalize)

    def initSubIndexes(self):
        self.img_subfix1 = "_img1.png"
        self.img_subfix2 = "_img2.png"
        self.flo_subfix = "_flow.flo"

    def loadIndexedSample(self, indx):
        if(indx < 10):
            fileindx = "0000"+str(indx)
        elif(indx < 100):
            fileindx = "000"+str(indx)
        elif(indx < 1000):
            fileindx = "00"+str(indx)
        elif(indx < 10000):
            fileindx = "0"+str(indx)
        else:
            fileindx = str(indx)

        img1dir = self.data_dir+"/"+fileindx+self.img_subfix1
        img2dir = self.data_dir+"/"+fileindx+self.img_subfix2
        flodir = self.data_dir+"/"+fileindx+self.flo_subfix

        #images are one channel only
        img1 = flowutils.read(img1dir)
        img1 = np.stack([img1, img1, img1], axis=-1)
        img2 = flowutils.read(img2dir)
        img2 = np.stack([img2, img2, img2], axis=-1)
        flow = flowutils.read(flodir)
        
        return img1, img2, flow

class SquareColorDatasetManager(ChairsDatasetManager) :

    def __init__(self, data_dir, train_ext_split, valid_ext_split, batchsize, shuffle=True, fragmentate=None, resize=None, load_in_ram=False, valid_batch_size=1, normalize=False):        
        super(SquareColorDatasetManager, self).__init__(data_dir, train_ext_split, valid_ext_split, batchsize, shuffle=shuffle, fragmentate=fragmentate, resize=resize, load_in_ram=load_in_ram, valid_batch_size=valid_batch_size, normalize=normalize)

    def initSubIndexes(self):
        self.img_subfix1 = "_img1.png"
        self.img_subfix2 = "_img2.png"
        self.flo_subfix = "_flow.flo"
