# coding=utf-8

import serial_flownet as snet
import flow_models_base as fmods 
import dataset_utils as fds
import os
import time

#windows
#dataset_path = "C:\LinuxNexum\Documents\FlyingChairs_release\data"
#linux server k40
dataset_path = "/home/ivan/Datasets/FlyingChairs_release/data"
#turriza 1080
#dataset_path = ""

lr_forall = 0.000001
examples = 400
fragments = 5
epocs = 50
today = time.strftime("%Y-%m-%d")
optimizer = "ADAM"

traink1 = [[1, 320]]
valk1 = [[321, 400]]

traink2 = [[1, 240], [321, 400]]
valk2 = [[241, 320]]

traink3 = [[1, 160], [241, 400]]
valk3 = [[161, 240]]

traink4 = [[1, 80], [161, 400]]
valk4 = [[81, 160]]

traink5 = [[81, 400]]
valk5 = [[1, 80]]

train1000 = [[1, 320], [401, 1080]]
valk1000 = [[321, 350]]

#restringido usar del 1122 al 1214

train1000 = [[1, 320], [401, 1080]]

train5000 = [[1, 320], [401, 1080], [2001, 6000]]
train5000_2 = [[6001, 11000]]

train10000 = [[1, 320], [401, 1080], [2001, 11000]]



name_model = "SNET_Adam_L1LOSS_5000EX_B20_"+fmods.FlowNetBrickModel5.name_type+"_LR_0001"
models_ckpt_paths = ["N1"]
train_schedule = {"epochs":4, "valid_step":1, "optimizer":"ADAM", "learning_rate": 0.00001, "cost_log_write_batch": 2, "cost_limit_examples": 2}
save_schedule = {"save_epoch":1, "save_batch_step":50}
chairs_ds = fds.ChairsDatasetManager(dataset_path, train5000_2, valk1000, 20, shuffle=False, load_in_ram=True)
mflownet = snet.FlowNetStackManager(name_model)
mods = [fmods.FlowNetBrickModel5(), fmods.FlowNetBrickModel5()]
#fix first
trainable_list = [False, True]

shape = chairs_ds.getFinalShape() #height, width
mflownet.model_build(shape[0], shape[1], mods, last_trainable=True, trainables_list=trainable_list)
mflownet.init_session()
mflownet.model_session_chkpt_restore(models_ckpt_paths)
#mflownet.train(chairs_ds, train_schedule, save_schedule)
mflownet.train2(chairs_ds, train_schedule, save_schedule) #fix gpu lack of memory
mflownet.close_session()